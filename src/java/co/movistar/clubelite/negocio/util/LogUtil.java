/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.util;

import co.movistar.clubelite.negocio.excepciones.ClubEliteException;

/**
 *
 * @author Lord_Nightmare
 */
public class LogUtil {

    public static void error(Throwable ex) {
        ex.printStackTrace(System.err);
    }

    public static void error(ClubEliteException ex) {
        if (ex.getCodigo() >= 0) {
            System.out.println(ex.getMensaje());
            return;
        }
        ex.printStackTrace(System.err);
    }

    public static void info(String mensaje) {
        System.out.println(mensaje);
    }
}
