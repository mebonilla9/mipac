/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.dao.PuntosAgentesDAO;
import co.movistar.clubelite.persistencia.vo.PuntosAgentes;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class PuntosAgentesDelegado extends GenericoDelegado<PuntosAgentes> {

    private final PuntosAgentesDAO puntosAgentesDAO;

    public PuntosAgentesDelegado(Connection cnn, AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn, auditoria);
        puntosAgentesDAO = new PuntosAgentesDAO(cnn);
        genericoDAO = puntosAgentesDAO;
    }
    
    public List<PuntosAgentes> consultarPuntosCanal(String canal, String cedula) throws ClubEliteException{
        try {
            return puntosAgentesDAO.consultar(canal, cedula);
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
