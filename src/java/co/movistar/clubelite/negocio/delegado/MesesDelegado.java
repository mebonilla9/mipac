/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.MesesDAO;
import co.movistar.clubelite.persistencia.vo.Meses;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class MesesDelegado extends GenericoDelegado<Meses> {

    private final MesesDAO mesesDAO;

    public MesesDelegado(Connection cnn, AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn, auditoria);
        mesesDAO = new MesesDAO(cnn);
        genericoDAO = mesesDAO;
    }
    
    public void registrarMeses(List<Meses> listaMeses) throws ClubEliteException{
        try {
            mesesDAO.reiniciarMeses();
            for (Meses mes : listaMeses) {
                mesesDAO.insertar(mes);
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_INSERTAR);
        }
    }

}
