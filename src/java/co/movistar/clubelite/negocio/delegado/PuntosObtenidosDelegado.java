/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.dao.PuntosObtenidosDAO;
import co.movistar.clubelite.persistencia.vo.PuntosObtenidos;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class PuntosObtenidosDelegado extends GenericoDelegado<PuntosObtenidos> {

    private final PuntosObtenidosDAO puntosObtenidosDAO;

    public PuntosObtenidosDelegado(Connection cnn, AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn, auditoria);
        puntosObtenidosDAO = new PuntosObtenidosDAO(cnn);
        genericoDAO = puntosObtenidosDAO;
    }
    
    public List<PuntosObtenidos> consultarCanalesObtenidos(Usuarios uActual) throws ClubEliteException{
        try {
            return puntosObtenidosDAO.consultarCanalesObtenidos(uActual.getCedula());
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
