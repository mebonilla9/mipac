/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dao.ValidezDAO;
import co.movistar.clubelite.persistencia.vo.Validez;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class ValidezDelegado extends GenericoDelegado<Validez> {

    private final ValidezDAO validezDAO;

    public ValidezDelegado(Connection cnn,AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn,auditoria);
        validezDAO = new ValidezDAO(cnn);
        genericoDAO = validezDAO;
    }
    
    public List<Validez> consultarActivos() throws ClubEliteException{
        try {
            return validezDAO.consultarActivos();
        } catch (SQLException e) {
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }
   

}
