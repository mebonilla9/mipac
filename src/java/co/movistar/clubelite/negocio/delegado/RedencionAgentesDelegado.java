/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.RedencionAgentesDAO;
import co.movistar.clubelite.persistencia.vo.RedencionAgentes;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.vo.PuntosRedimidos;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class RedencionAgentesDelegado extends GenericoDelegado<RedencionAgentes> {

    private final RedencionAgentesDAO redencionAgentesDAO;

    public RedencionAgentesDelegado(Connection cnn,AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn,auditoria);
        redencionAgentesDAO = new RedencionAgentesDAO(cnn);
        genericoDAO = redencionAgentesDAO;
    }

    public void aprobarRedencion(Long idAprob, String valor) throws ClubEliteException{
        try {
            RedencionAgentes redem = redencionAgentesDAO.consultar(idAprob);
            redem.setAutorizacion(valor);
            redencionAgentesDAO.editar(redem);
        } catch (SQLException e) {
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_MODIFICAR);
        }
    }

    public List<RedencionAgentes> consultarMisRedenciones(String nombre) throws ClubEliteException {
        try {
            return redencionAgentesDAO.consultarMisRedenciones(nombre);
        } catch (SQLException e) {
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_MODIFICAR);
        }
    }
   

}
