/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.crud.IGenericoDAO;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public abstract class GenericoDelegado<T> {

    protected Connection cnn;
    protected IGenericoDAO genericoDAO;
    protected boolean confirmar = true;

    public GenericoDelegado(Connection cnn, AuditoriaDTO auditoriaDTO) throws ClubEliteException {
        this.cnn = cnn;
    }

    public void insertar(T entidad) throws ClubEliteException {
        try {
            genericoDAO.insertar(entidad);
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_INSERTAR);
        }
    }

    public void editar(T entidad) throws ClubEliteException {
        try {
            genericoDAO.editar(entidad);

        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_MODIFICAR);
        }
    }

    public List<T> consultar() throws ClubEliteException {
        try {
            return genericoDAO.consultar();
        } catch (SQLException ex) {
            System.out.println("--------------- excepcion ----------------");
            System.out.println(ex.getMessage());
            System.out.println("--------------- excepcion ----------------");
            ex.printStackTrace(System.err);
            System.out.println("--------------- excepcion ----------------");
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

    public T consultar(long id) throws ClubEliteException {
        try {
            return (T) genericoDAO.consultar(id);
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
