/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dao.PuntosRedimidosDAO;
import co.movistar.clubelite.persistencia.vo.PuntosRedimidos;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import java.sql.Connection;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class PuntosRedimidosDelegado extends GenericoDelegado<PuntosRedimidos> {

    private final PuntosRedimidosDAO PuntosRedimidosDAO;

    public PuntosRedimidosDelegado(Connection cnn,AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn,auditoria);
        PuntosRedimidosDAO = new PuntosRedimidosDAO(cnn);
        genericoDAO = PuntosRedimidosDAO;
    }
   

}
