/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dao.VwHistoriapuntosDAO;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.vo.VwHistoriapuntos;
import java.sql.Connection;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class VwHistoriapuntosDelegado extends GenericoViewDelegado<VwHistoriapuntos> {

    private final VwHistoriapuntosDAO vwhistoriapuntosDAO;

    public VwHistoriapuntosDelegado(Connection cnn, AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn, auditoria);
        vwhistoriapuntosDAO = new VwHistoriapuntosDAO(cnn);
        genericoViewDAO = vwhistoriapuntosDAO;
    }

}
