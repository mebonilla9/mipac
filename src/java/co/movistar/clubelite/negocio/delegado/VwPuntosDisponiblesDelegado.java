/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.dao.VwPuntosDisponiblesDAO;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import co.movistar.clubelite.persistencia.vo.VwPuntosDisponibles;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class VwPuntosDisponiblesDelegado extends GenericoViewDelegado<VwPuntosDisponibles> {

    private final VwPuntosDisponiblesDAO vwPuntosDisponiblesDAO;

    public VwPuntosDisponiblesDelegado(Connection cnn, AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn, auditoria);
        vwPuntosDisponiblesDAO = new VwPuntosDisponiblesDAO(cnn);
        genericoViewDAO = vwPuntosDisponiblesDAO;
    }

    public List<VwPuntosDisponibles> consultar(Usuarios uActual) throws ClubEliteException {
        try {
            return vwPuntosDisponiblesDAO.consultar(uActual.getCedula());
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }
    
    public VwPuntosDisponibles consultarPuntosCanal(Usuarios uActual, String canal) throws ClubEliteException {
        try {
            return vwPuntosDisponiblesDAO.consultarPorCanal(uActual.getCedula(), canal);
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
