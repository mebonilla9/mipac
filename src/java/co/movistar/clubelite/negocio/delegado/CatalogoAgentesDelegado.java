/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.dao.CatalogoAgentesDAO;
import co.movistar.clubelite.persistencia.vo.CatalogoAgentes;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import co.movistar.clubelite.persistencia.vo.VwPuntosDisponibles;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class CatalogoAgentesDelegado extends GenericoDelegado<CatalogoAgentes> {

    private final CatalogoAgentesDAO catalogoAgentesDAO;

    public CatalogoAgentesDelegado(Connection cnn, AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn, auditoria);
        catalogoAgentesDAO = new CatalogoAgentesDAO(cnn);
        genericoDAO = catalogoAgentesDAO;
    }

    public List<CatalogoAgentes> catalogoDisponibles(Usuarios uActual, String canal) throws ClubEliteException {
        try {
            VwPuntosDisponibles disponible = new VwPuntosDisponiblesDelegado(cnn, null).consultarPuntosCanal(uActual, canal);
            if (disponible == null) {
                throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
            }
            return catalogoAgentesDAO.catalogoDisponibles(disponible.getDisponibles(), canal);
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }
}
