/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.edu.intecap.minibancolibreria.negocio.util.CryptoUtil;
import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.dao.UsuariosDAO;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class UsuariosDelegado extends GenericoDelegado<Usuarios> {

    private final UsuariosDAO usuariosDAO;

    public UsuariosDelegado(Connection cnn, AuditoriaDTO auditoria) throws ClubEliteException {
        super(cnn, auditoria);
        usuariosDAO = new UsuariosDAO(cnn);
        genericoDAO = usuariosDAO;
    }

    public void insertarNuevoUsuario(Usuarios usuario) throws ClubEliteException {
        try {
            String cUno = CryptoUtil.cifrarContrasena(usuario.getCedula(), "256");
            String cDos = CryptoUtil.cifrarContrasena(cUno, "384");
            usuario.setContrasena(cDos);
            usuariosDAO.insertar(usuario);
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_INSERTAR);
        }
    }

    public Usuarios consultaLogin(String cedula, String contrasena) throws ClubEliteException {
        try {
            return usuariosDAO.consultar(cedula, contrasena);
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

    public List<Usuarios> consultarKams() throws ClubEliteException {
        try {
            return usuariosDAO.consultarKams();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
