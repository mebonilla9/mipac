/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.delegado;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.crud.IGenericoViewDao;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Lord_Nightmare
 */
public abstract class GenericoViewDelegado<T> {

    protected Connection cnn;
    protected IGenericoViewDao genericoViewDAO;
    protected boolean confirmar = true;

    public GenericoViewDelegado(Connection cnn, AuditoriaDTO auditoriaDTO) throws ClubEliteException {
        this.cnn = cnn;
    }

    public List<T> consultar() throws ClubEliteException {
        try {
            return genericoViewDAO.consultar();
        } catch (SQLException ex) {
            ConexionBD.rollback(cnn);
            throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
        }
    }

}
