/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.edu.intecap.minibancolibreria.negocio.util.CryptoUtil;
import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.UsuariosDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "UsuariosServlet", urlPatterns = {
    ERutas.Usuarios.INSERTAR_USUARIOS,
    ERutas.Usuarios.MODIFICAR_USUARIOS,
    ERutas.Usuarios.CONSULTAR_USUARIOS,
    ERutas.Usuarios.BUSCAR_USUARIOS,
    ERutas.Usuarios.BUSCAR_KAMS
})
public class UsuariosServlet extends ServletEstandarJSON {

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        UsuariosDelegado usuariosDelegado = new UsuariosDelegado(cnn, null);
        switch (accion) {
            case ERutas.Usuarios.INSERTAR_USUARIOS:
                Usuarios uRegistrar = getUsuario(request);
                if (uRegistrar.getContrasena() == null) {
                    usuariosDelegado.insertarNuevoUsuario(uRegistrar);
                } else {
                    usuariosDelegado.insertar(uRegistrar);
                }
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.Usuarios.MODIFICAR_USUARIOS:
                Usuarios uEditar = getUsuario(request);
                Usuarios uConsultar = usuariosDelegado.consultar(uEditar.getIdUsuarios());
                if (!uEditar.getContrasena().equals(uConsultar.getContrasena())) {
                    String cifrado = CryptoUtil.cifrarContrasena(uEditar.getContrasena(), "384");
                    uEditar.setContrasena(cifrado);
                }
                usuariosDelegado.editar(uEditar);
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.Usuarios.CONSULTAR_USUARIOS:
                List<Usuarios> listaPuntosAgentes = usuariosDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.Usuarios.BUSCAR_USUARIOS:
                Long id = Long.parseLong(request.getParameter("id"));
                if (id == null) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                if (id == 0) {
                    id = ((Usuarios) request.getSession().getAttribute("usuario")).getIdUsuarios();
                }
                Usuarios usuario = usuariosDelegado.consultar(id);
                if (usuario != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(usuario);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.Usuarios.BUSCAR_KAMS:
                List<Usuarios> usuariosKam = usuariosDelegado.consultarKams();
                if (!usuariosKam.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(usuariosKam);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }

    private Usuarios getUsuario(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<Usuarios>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }
}
