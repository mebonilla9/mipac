/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
public abstract class ServletEstandarJSON extends GenericoServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String accion) throws ServletException, IOException {
        procesar(request, response, accion);
    }

    public void procesar(HttpServletRequest request, HttpServletResponse response, String accion) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        Connection cnn = null;
        try (PrintWriter out = response.getWriter()) {
            RespuestaDTO respuesta;
            AuditoriaDTO auditoria = null;
            try {
                auditoria = getAuditoria(request);
                cnn = ConexionBD.conectar();
                respuesta = procesarPeticionJSON(request, response, accion, auditoria, cnn);
                cnn.commit();
            } catch (JsonSyntaxException ex) {
                respuesta = new RespuestaDTO(EMensajes.ERROR_JSON);
            } catch (ClubEliteException ex) {
                LogUtil.error(ex);
                respuesta = new RespuestaDTO(EMensajes.ERROR_CONSULTAR);
                respuesta.setCodigo(ex.getCodigo());
                respuesta.setMensaje(ex.getMensaje());
            } catch (Throwable ex) {
                LogUtil.error(ex);
                respuesta = new RespuestaDTO(EMensajes.ERROR_FATAL);
            } finally {
                ConexionBD.rollback(cnn);
                ConexionBD.desconectar(cnn);
            }
            out.println(new Gson().toJson(respuesta));
        }
    }

    public abstract RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException;

    protected String getParametros(HttpServletRequest request) throws ClubEliteException {

        String json;
        try {
            json = request.getReader().readLine();
        } catch (IOException e) {
            throw new ClubEliteException(EMensajes.ERROR_JSON);
        }
        return json;
    }
}
