/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.PuntosAgentesDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.PuntosAgentes;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "PuntosAgentesServlet", urlPatterns = {
    ERutas.PuntosAgentes.INSERTAR_PUNTOS_AGENTES,
    ERutas.PuntosAgentes.MODIFICAR_PUNTOS_AGENTES,
    ERutas.PuntosAgentes.CONSULTAR_PUNTOS_AGENTES,
    ERutas.PuntosAgentes.BUSCAR_PUNTOS_AGENTES,
    ERutas.PuntosAgentes.PUNTOS_AGENTES_CANAL
})
public class PuntosAgentesServlet extends ServletEstandarJSON {

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        PuntosAgentesDelegado cAgentesDelegado = new PuntosAgentesDelegado(cnn, null);
        switch (accion) {
            case ERutas.PuntosAgentes.INSERTAR_PUNTOS_AGENTES:
                cAgentesDelegado.insertar(getPuntosAgentes(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.PuntosAgentes.MODIFICAR_PUNTOS_AGENTES:
                cAgentesDelegado.editar(getPuntosAgentes(request));
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.PuntosAgentes.CONSULTAR_PUNTOS_AGENTES:
                List<PuntosAgentes> listaPuntosAgentes = cAgentesDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.PuntosAgentes.BUSCAR_PUNTOS_AGENTES:
                String id = request.getParameter("id");
                if (id == null || id.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                PuntosAgentes puntosAgentes = cAgentesDelegado.consultar(Long.parseLong(id));
                if (puntosAgentes != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(puntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.PuntosAgentes.PUNTOS_AGENTES_CANAL:
                String canal = request.getParameter("canal");
                if (canal == null || canal.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                Usuarios usuarioActual = (Usuarios) request.getSession().getAttribute("usuario");
                if (usuarioActual == null) {
                    throw new ClubEliteException(EMensajes.NO_RESULTADOS);
                }
                List<PuntosAgentes> puntosCanales = cAgentesDelegado.consultarPuntosCanal(canal, usuarioActual.getCedula());
                if (!puntosCanales.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(puntosCanales);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }

    private PuntosAgentes getPuntosAgentes(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<PuntosAgentes>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }

}
