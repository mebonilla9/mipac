/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.ValidezDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.Validez;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "ValidezServlet", urlPatterns = {
    ERutas.Validez.INSERTAR_VALIDEZ,
    ERutas.Validez.MODIFICAR_VALIDEZ,
    ERutas.Validez.CONSULTAR_VALIDEZ,
    ERutas.Validez.BUSCAR_VALIDEZ
})
public class ValidezServlet extends ServletEstandarJSON {

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        ValidezDelegado validezDelegado = new ValidezDelegado(cnn, null);
        switch (accion) {
            case ERutas.Validez.INSERTAR_VALIDEZ:
                validezDelegado.insertar(getValidez(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.Validez.MODIFICAR_VALIDEZ:
                validezDelegado.editar(getValidez(request));
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.Validez.CONSULTAR_VALIDEZ:
                List<Validez> listaPuntosAgentes = validezDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.Validez.BUSCAR_VALIDEZ:
                String id = request.getParameter("id");
                if (id == null || id.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                Validez validez = validezDelegado.consultar(Long.parseLong(id));
                if (validez != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(validez);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }

    private Validez getValidez(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<Validez>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }

}
