/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.edu.intecap.minibancolibreria.negocio.util.CryptoUtil;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.UsuariosDelegado;
import co.movistar.clubelite.negocio.delegado.ValidezDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import co.movistar.clubelite.persistencia.vo.Validez;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "AutenticacionServlet", urlPatterns = {
    ERutas.Index.INICIAR,
    ERutas.Index.AUTENTICAR,
    ERutas.Index.CERRAR_SESION,
    ERutas.Dashboard.CARGAR
})
public class AutenticacionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession sesion;
            switch (request.getServletPath()) {
                case ERutas.Index.INICIAR:
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                    break;
                case ERutas.Index.AUTENTICAR:
                    iniciarSesion(request, response);
                    break;
                case ERutas.Index.CERRAR_SESION:
                    sesion = request.getSession();
                    sesion.invalidate();
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                    break;
                case ERutas.Dashboard.CARGAR:
                    sesion = request.getSession();
                    if (sesion.getAttribute("usuario") == null) {
                        response.sendRedirect(request.getContextPath() + ERutas.Index.INICIAR);
                    }
                    request.getRequestDispatcher("/home.jsp").forward(request, response);
                default:
                    break;
            }
        } catch (ClubEliteException e) {
            LogUtil.error(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void iniciarSesion(HttpServletRequest request, HttpServletResponse response) throws ClubEliteException, ServletException, IOException {

        String usuario = request.getParameter("cedula");
        String contrasena = request.getParameter("contrasena");

        if (usuario == null || contrasena == null) {
            System.out.println("---------------------------- usuario o contraseña nulos ----------------------------");
            request.setAttribute("error_usuario", "Usuario no encontrado, intente nuevamente!");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
        }

        HttpSession sesion = request.getSession();

        Connection cnn = ConexionBD.conectar();
        Usuarios usuarioAutorizado = new UsuariosDelegado(cnn, null).consultaLogin(usuario, CryptoUtil.cifrarContrasena(contrasena, "384"));

        if (usuarioAutorizado.getIdUsuarios() == null) {
            System.out.println("---------------------------- usuario no encontrado ----------------------------");
            request.setAttribute("error_usuario", "Usuario no encontrado, intente nuevamente!");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
        }

        sesion.setAttribute("usuario", usuarioAutorizado);
        List<Validez> validos = new ValidezDelegado(cnn, null).consultar();
        System.out.println("---------------------------- validez cargada con "+validos.size()+" items ----------------------------");
        sesion.setAttribute("validos", validos);

        if (sesion.getAttribute("usuario") != null) {
            System.out.println("---------------------------- usuario en sesion ----------------------------");
            response.sendRedirect(request.getContextPath() + ERutas.Dashboard.CARGAR);
        }
    }
}
