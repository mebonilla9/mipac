/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.PuntosRedimidosDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.PuntosRedimidos;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "PuntosRedimidosServlet", urlPatterns = {
    ERutas.PuntosRedimidos.INSERTAR_PUNTOS_REDIMIDOS,
    ERutas.PuntosRedimidos.MODIFICAR_PUNTOS_REDIMIDOS,
    ERutas.PuntosRedimidos.CONSULTAR_PUNTOS_REDIMIDOS,
    ERutas.PuntosRedimidos.BUSCAR_PUNTOS_REDIMIDOS
})
public class PuntosRedimidosServlet extends ServletEstandarJSON{

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        PuntosRedimidosDelegado pRedimidosDelegado = new PuntosRedimidosDelegado(cnn, null);
        switch (accion) {
            case ERutas.PuntosRedimidos.INSERTAR_PUNTOS_REDIMIDOS:
                pRedimidosDelegado.insertar(getPuntosRedimidos(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.PuntosRedimidos.MODIFICAR_PUNTOS_REDIMIDOS:
                pRedimidosDelegado.editar(getPuntosRedimidos(request));
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.PuntosRedimidos.CONSULTAR_PUNTOS_REDIMIDOS:
                List<PuntosRedimidos> listaPuntosAgentes = pRedimidosDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.PuntosRedimidos.BUSCAR_PUNTOS_REDIMIDOS:
                String id = request.getParameter("id");
                if (id == null || id.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                PuntosRedimidos puntosAgentes = pRedimidosDelegado.consultar(Long.parseLong(id));
                if (puntosAgentes != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(puntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }

    private PuntosRedimidos getPuntosRedimidos(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<PuntosRedimidos>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }
    
}
