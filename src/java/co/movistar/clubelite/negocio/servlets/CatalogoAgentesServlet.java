/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.CatalogoAgentesDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.CatalogoAgentes;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
@WebServlet(name = "CatalogoAgentesServlet", urlPatterns = {
    ERutas.CatalogoAgentes.INSERTAR_CATALOGO_AGENTES,
    ERutas.CatalogoAgentes.MODIFICAR_CATALOGO_AGENTES,
    ERutas.CatalogoAgentes.CONSULTAR_CATALOGO_AGENTES,
    ERutas.CatalogoAgentes.BUSCAR_CATALOGO_AGENTES
})
public class CatalogoAgentesServlet extends ServletEstandarJSON {

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        CatalogoAgentesDelegado cAgentesDelegado = new CatalogoAgentesDelegado(cnn, null);
        switch (accion) {
            case ERutas.CatalogoAgentes.INSERTAR_CATALOGO_AGENTES:
                cAgentesDelegado.insertar(getCatalogoAgentes(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.CatalogoAgentes.MODIFICAR_CATALOGO_AGENTES:
                cAgentesDelegado.editar(getCatalogoAgentes(request));
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.CatalogoAgentes.CONSULTAR_CATALOGO_AGENTES:
                List<CatalogoAgentes> listaCatalogoAgentes = null;
                String puntos = request.getParameter("puntos");
                if (puntos != null) {
                    Usuarios uActual = (Usuarios) request.getSession().getAttribute("usuario");
                    listaCatalogoAgentes = cAgentesDelegado.catalogoDisponibles(uActual, puntos);
                } else {
                    listaCatalogoAgentes = cAgentesDelegado.consultar();
                }
                if (!listaCatalogoAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaCatalogoAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.CatalogoAgentes.BUSCAR_CATALOGO_AGENTES:
                String id = request.getParameter("id");
                if (id == null || id.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                CatalogoAgentes catalogoAgentes = cAgentesDelegado.consultar(Long.parseLong(id));
                if (catalogoAgentes != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(catalogoAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }

    private CatalogoAgentes getCatalogoAgentes(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<CatalogoAgentes>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }

}
