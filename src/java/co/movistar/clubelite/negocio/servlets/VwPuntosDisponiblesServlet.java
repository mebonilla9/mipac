/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.VwPuntosDisponiblesDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import co.movistar.clubelite.persistencia.vo.VwPuntosDisponibles;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "VwPuntosDisponiblesServlet", urlPatterns = {
    ERutas.PuntosDisponibles.CONSULTAR_PUNTOS_DISPONIBLES,
    ERutas.PuntosDisponibles.CONSULTAR_PUNTOS_CANAL
})
public class VwPuntosDisponiblesServlet extends ServletEstandarJSON {

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        VwPuntosDisponiblesDelegado vwPuntosDisponiblesDelegado = new VwPuntosDisponiblesDelegado(cnn, null);
        switch (accion) {
            case ERutas.PuntosDisponibles.CONSULTAR_PUNTOS_DISPONIBLES:
                List<VwPuntosDisponibles> listaPuntosAgentes = vwPuntosDisponiblesDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.PuntosDisponibles.CONSULTAR_PUNTOS_CANAL:
                VwPuntosDisponibles puntosCanal;
                String canal = request.getParameter("canal");
                if (canal == null) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                Usuarios uActual = (Usuarios) request.getSession().getAttribute("usuario");
                puntosCanal = vwPuntosDisponiblesDelegado.consultarPuntosCanal(uActual, canal);
                if (puntosCanal != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(puntosCanal);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }

}
