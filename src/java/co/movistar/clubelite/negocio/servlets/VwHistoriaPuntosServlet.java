/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.VwHistoriapuntosDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.VwHistoriapuntos;
import java.sql.Connection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
public class VwHistoriaPuntosServlet extends ServletEstandarJSON{

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        VwHistoriapuntosDelegado vwHistoriapuntosDelegado = new VwHistoriapuntosDelegado(cnn, null);
        switch (accion) {
            case ERutas.PuntosDisponibles.CONSULTAR_PUNTOS_DISPONIBLES:
                List<VwHistoriapuntos> listaPuntosAgentes = vwHistoriapuntosDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }
    
}
