/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.PuntosRedimidosDelegado;
import co.movistar.clubelite.negocio.delegado.RedencionAgentesDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.PuntosRedimidos;
import co.movistar.clubelite.persistencia.vo.RedencionAgentes;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "RedencionAgentesServlet", urlPatterns = {
    ERutas.RedencionAgentes.INSERTAR_REDENCION_AGENTES,
    ERutas.RedencionAgentes.MODIFICAR_REDENCION_AGENTES,
    ERutas.RedencionAgentes.CONSULTAR_REDENCION_AGENTES,
    ERutas.RedencionAgentes.BUSCAR_REDENCION_AGENTES,
    ERutas.RedencionAgentes.APROBAR_REDENCION_AGENTES,
    ERutas.RedencionAgentes.ENCONTRAR_REDENCION_AGENTES,
    ERutas.RedencionAgentes.INSERTAR_REDENCION_NUEVO
})
public class RedencionAgentesServlet extends ServletEstandarJSON {

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        RedencionAgentesDelegado reAgentesDelegado = new RedencionAgentesDelegado(cnn, null);
        switch (accion) {
            case ERutas.RedencionAgentes.INSERTAR_REDENCION_AGENTES:
                reAgentesDelegado.insertar(getRedencionAgentes(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.RedencionAgentes.MODIFICAR_REDENCION_AGENTES:
                reAgentesDelegado.editar(getRedencionAgentes(request));
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.RedencionAgentes.CONSULTAR_REDENCION_AGENTES:
                List<RedencionAgentes> listaPuntosAgentes = reAgentesDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.RedencionAgentes.BUSCAR_REDENCION_AGENTES:
                String id = request.getParameter("id");
                if (id == null || id.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                RedencionAgentes puntosAgentes = reAgentesDelegado.consultar(Long.parseLong(id));
                if (puntosAgentes != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(puntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.RedencionAgentes.APROBAR_REDENCION_AGENTES:
                String idAprob = request.getParameter("id");
                String valor = request.getParameter("valor");
                reAgentesDelegado.aprobarRedencion(Long.parseLong(idAprob), valor);
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.RedencionAgentes.ENCONTRAR_REDENCION_AGENTES:
                String nombre = ((Usuarios) request.getSession().getAttribute("usuario")).getNombre();
                List<RedencionAgentes> misRedenciones = reAgentesDelegado.consultarMisRedenciones(nombre);
                if (!misRedenciones.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(misRedenciones);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.RedencionAgentes.INSERTAR_REDENCION_NUEVO:
                RedencionAgentes redencion = getRedencionAgentes(request);
                redencion.setCedula(((Usuarios) request.getSession().getAttribute("usuario")).getCedula());
                redencion.setFecha(new Date());
                redencion.setValor(0L);
                reAgentesDelegado.insertar(redencion);

                PuntosRedimidos pr = new PuntosRedimidos();
                pr.setFecha(new Date());
                pr.setPago(0L);
                pr.setRedimidos(redencion.getPuntos());
                pr.setCanal(redencion.getCanal());
                pr.setCedula(redencion.getCedula());

                new PuntosRedimidosDelegado(cnn, null).insertar(pr);
                
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
        }
        return respuesta;
    }

    private RedencionAgentes getRedencionAgentes(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<RedencionAgentes>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }

}
