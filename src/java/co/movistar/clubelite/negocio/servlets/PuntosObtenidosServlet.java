/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.PuntosObtenidosDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.PuntosObtenidos;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "PuntosObtenidosServlet", urlPatterns = {
    ERutas.PuntosObtenidos.INSERTAR_PUNTOS_OBTENIDOS,
    ERutas.PuntosObtenidos.MODIFICAR_PUNTOS_OBTENIDOS,
    ERutas.PuntosObtenidos.CONSULTAR_PUNTOS_OBTENIDOS,
    ERutas.PuntosObtenidos.BUSCAR_PUNTOS_OBTENIDOS,
    ERutas.PuntosObtenidos.CANAL_PUNTOS_OBTENIDOS
})
public class PuntosObtenidosServlet extends ServletEstandarJSON {

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        PuntosObtenidosDelegado pObtenidosDelegado = new PuntosObtenidosDelegado(cnn, null);
        switch (accion) {
            case ERutas.PuntosObtenidos.INSERTAR_PUNTOS_OBTENIDOS:
                pObtenidosDelegado.insertar(getPuntosObtenidos(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.PuntosObtenidos.MODIFICAR_PUNTOS_OBTENIDOS:
                pObtenidosDelegado.editar(getPuntosObtenidos(request));
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.PuntosObtenidos.CONSULTAR_PUNTOS_OBTENIDOS:
                List<PuntosObtenidos> listaPuntosAgentes = pObtenidosDelegado.consultar();
                if (!listaPuntosAgentes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaPuntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.PuntosObtenidos.BUSCAR_PUNTOS_OBTENIDOS:
                String id = request.getParameter("id");
                if (id == null || id.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                PuntosObtenidos puntosAgentes = pObtenidosDelegado.consultar(Long.parseLong(id));
                if (puntosAgentes != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(puntosAgentes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.PuntosObtenidos.CANAL_PUNTOS_OBTENIDOS:
                Usuarios uActual = (Usuarios) request.getSession().getAttribute("usuario");
                List<PuntosObtenidos> canalesPuntos = pObtenidosDelegado.consultarCanalesObtenidos(uActual);
                if (!canalesPuntos.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(canalesPuntos);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
        }
        return respuesta;
    }

    private PuntosObtenidos getPuntosObtenidos(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<PuntosObtenidos>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }

}
