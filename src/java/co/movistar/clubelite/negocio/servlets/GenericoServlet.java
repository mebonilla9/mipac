/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.movistar.clubelite.negocio.util.LogUtil;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public abstract class GenericoServlet extends HttpServlet {

    protected abstract void processRequest(HttpServletRequest request, HttpServletResponse response, String accion) throws ServletException, IOException;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        interceptarPeticion(request, response);
    }

    private void interceptarPeticion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        String accion = request.getServletPath();
        processRequest(request, response, accion);
        liminarMemoria();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        interceptarPeticion(request, response);
    }

    private void liminarMemoria() {
        try {
            System.gc();
        } catch (Throwable e) {
            LogUtil.error(e);
        }
    }

    protected AuditoriaDTO getAuditoria(HttpServletRequest request) throws ClubEliteException {
        return null;
    }

    protected void actualizarToken(AuditoriaDTO auditoriaDTO, RespuestaDTO respuesta, String accion) {

    }

}
