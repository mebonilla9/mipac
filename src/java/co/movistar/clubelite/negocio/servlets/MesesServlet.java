/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.servlets;

import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.constantes.ERutas;
import co.movistar.clubelite.negocio.delegado.MesesDelegado;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import co.movistar.clubelite.persistencia.dto.AuditoriaDTO;
import co.movistar.clubelite.persistencia.dto.RespuestaDTO;
import co.movistar.clubelite.persistencia.vo.Meses;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lord_Nightmare
 */
@WebServlet(name = "MesesServlet", urlPatterns = {
    ERutas.Meses.INSERTAR_MESES,
    ERutas.Meses.MODIFICAR_MESES,
    ERutas.Meses.CONSULTAR_MESES,
    ERutas.Meses.BUSCAR_MESES,
    ERutas.Meses.REGISTRAR_MESES
})
public class MesesServlet extends ServletEstandarJSON{

    @Override
    public RespuestaDTO procesarPeticionJSON(HttpServletRequest request, HttpServletResponse response, String accion, AuditoriaDTO auditoria, Connection cnn) throws ClubEliteException {
        RespuestaDTO respuesta = null;
        MesesDelegado mesesDelegado = new MesesDelegado(cnn, null);
        switch (accion) {
            case ERutas.Meses.INSERTAR_MESES:
                mesesDelegado.insertar(getMeses(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
            case ERutas.Meses.MODIFICAR_MESES:
                mesesDelegado.editar(getMeses(request));
                respuesta = new RespuestaDTO(EMensajes.MODIFICO);
                break;
            case ERutas.Meses.CONSULTAR_MESES:
                List<Meses> listaMes = mesesDelegado.consultar();
                if (!listaMes.isEmpty()) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(listaMes);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.Meses.BUSCAR_MESES:
                String id = request.getParameter("id");
                if (id == null || id.isEmpty()) {
                    throw new ClubEliteException(EMensajes.ERROR_CONSULTAR);
                }
                Meses meses = mesesDelegado.consultar(Long.parseLong(id));
                if (meses != null) {
                    respuesta = new RespuestaDTO(EMensajes.CONSULTO);
                    respuesta.setDatos(meses);
                } else {
                    respuesta = new RespuestaDTO(EMensajes.NO_RESULTADOS);
                }
                break;
            case ERutas.Meses.REGISTRAR_MESES:
                mesesDelegado.registrarMeses(getListaMeses(request));
                respuesta = new RespuestaDTO(EMensajes.INSERTO);
                break;
        }
        return respuesta;
    }

    private Meses getMeses(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<Meses>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }
    
    private List<Meses> getListaMeses(HttpServletRequest request) throws ClubEliteException {
        String json = getParametros(request);
        Type tipo = new TypeToken<List<Meses>>() {
        }.getType();
        return new Gson().fromJson(json, tipo);
    }
    
}
