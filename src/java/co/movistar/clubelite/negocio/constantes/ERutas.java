/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.negocio.constantes;

/**
 *
 * @author Lord_Nightmare
 */
public class ERutas {

    public static final class CatalogoAgentes {

        public static final String INSERTAR_CATALOGO_AGENTES = "/catalogoagentes/insertar";
        public static final String MODIFICAR_CATALOGO_AGENTES = "/catalogoagentes/editar";
        public static final String CONSULTAR_CATALOGO_AGENTES = "/catalogoagentes/consultar";
        public static final String BUSCAR_CATALOGO_AGENTES = "/catalogoagentes/buscar";
    }

    public static final class Meses {

        public static final String INSERTAR_MESES = "/meses/insertar";
        public static final String MODIFICAR_MESES = "/meses/editar";
        public static final String CONSULTAR_MESES = "/meses/consultar";
        public static final String BUSCAR_MESES = "/meses/buscar";
        public static final String REGISTRAR_MESES = "/meses/registrar";
    }

    public static final class PuntosAgentes {

        public static final String INSERTAR_PUNTOS_AGENTES = "/puntosagentes/insertar";
        public static final String MODIFICAR_PUNTOS_AGENTES = "/puntosagentes/editar";
        public static final String CONSULTAR_PUNTOS_AGENTES = "/puntosagentes/consultar";
        public static final String BUSCAR_PUNTOS_AGENTES = "/puntosagentes/buscar";
        public static final String PUNTOS_AGENTES_CANAL = "/puntosagentes/canal";
    }

    public static final class PuntosObtenidos {

        public static final String INSERTAR_PUNTOS_OBTENIDOS = "/puntosobtenidos/insertar";
        public static final String MODIFICAR_PUNTOS_OBTENIDOS = "/puntosobtenidos/editar";
        public static final String CONSULTAR_PUNTOS_OBTENIDOS = "/puntosobtenidos/consultar";
        public static final String BUSCAR_PUNTOS_OBTENIDOS = "/puntosobtenidos/buscar";
        public static final String CANAL_PUNTOS_OBTENIDOS = "/puntosobtenidos/canal";
    }

    public static final class PuntosRedimidos {

        public static final String INSERTAR_PUNTOS_REDIMIDOS = "/puntosredimidos/insertar";
        public static final String MODIFICAR_PUNTOS_REDIMIDOS = "/puntosredimidos/editar";
        public static final String CONSULTAR_PUNTOS_REDIMIDOS = "/puntosredimidos/consultar";
        public static final String BUSCAR_PUNTOS_REDIMIDOS = "/puntosredimidos/buscar";
    }

    public static final class RedencionAgentes {

        public static final String INSERTAR_REDENCION_AGENTES = "/redencionagentes/insertar";
        public static final String MODIFICAR_REDENCION_AGENTES = "/redencionagentes/editar";
        public static final String CONSULTAR_REDENCION_AGENTES = "/redencionagentes/consultar";
        public static final String BUSCAR_REDENCION_AGENTES = "/redencionagentes/buscar";
        public static final String APROBAR_REDENCION_AGENTES = "/redencionagentes/aprobar";
        public static final String ENCONTRAR_REDENCION_AGENTES = "/redencionagentes/encontrar";
        public static final String INSERTAR_REDENCION_NUEVO = "/redencionagentes/nuevo";
    }

    public static final class Usuarios {

        public static final String INSERTAR_USUARIOS = "/usuarios/insertar";
        public static final String MODIFICAR_USUARIOS = "/usuarios/editar";
        public static final String CONSULTAR_USUARIOS = "/usuarios/consultar";
        public static final String BUSCAR_USUARIOS = "/usuarios/buscar";
        public static final String BUSCAR_KAMS = "/usuarios/kam";
    }

    public static final class Validez {

        public static final String INSERTAR_VALIDEZ = "/validez/insertar";
        public static final String MODIFICAR_VALIDEZ = "/validez/editar";
        public static final String CONSULTAR_VALIDEZ = "/validez/consultar";
        public static final String BUSCAR_VALIDEZ = "/validez/buscar";
    }

    public static final class HistoriaPuntos {

        public static final String CONSULTAR_HISTORIA_PUNTOS = "/historiapuntos/consultar";
    }

    public static final class PuntosDisponibles {

        public static final String CONSULTAR_PUNTOS_DISPONIBLES = "/puntosdisponibles/consultar";
        public static final String CONSULTAR_PUNTOS_CANAL = "/puntosdisponibles/canal";
    }

    public static final class Index {

        public static final String INICIAR = "/ingresar";
        public static final String AUTENTICAR = "/autenticar";
        public static final String CERRAR_SESION = "/cerrar";
    }

    public static final class Dashboard {

        public static final String CARGAR = "/home";
    }
}
