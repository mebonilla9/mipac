package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class VwPuntosDisponibles implements Serializable {

    private String cedula;
    private String canal;
    private Long disponibles;

    public VwPuntosDisponibles() {
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * @return the disponibles
     */
    public Long getDisponibles() {
        return disponibles;
    }

    /**
     * @param disponibles the disponibles to set
     */
    public void setDisponibles(Long disponibles) {
        this.disponibles = disponibles;
    }

}
