package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class PuntosObtenidos implements Serializable {

    private Long idPuntosobtenidos;
    private String cedula;
    private Long puntos;
    private String mes;
    private String canal;

    public PuntosObtenidos() {
    }

    /**
     * @return the idPuntosobtenidos
     */
    public Long getIdPuntosobtenidos() {
        return idPuntosobtenidos;
    }

    /**
     * @param idPuntosobtenidos the idPuntosobtenidos to set
     */
    public void setIdPuntosobtenidos(Long idPuntosobtenidos) {
        this.idPuntosobtenidos = idPuntosobtenidos;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the puntos
     */
    public Long getPuntos() {
        return puntos;
    }

    /**
     * @param puntos the puntos to set
     */
    public void setPuntos(Long puntos) {
        this.puntos = puntos;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

}
