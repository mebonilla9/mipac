package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class Validez implements Serializable {

    private Long idValidez;
    private String tipo;
    private String validez;

    /**
     * @return the idValidez
     */
    public Long getIdValidez() {
        return idValidez;
    }

    /**
     * @param idValidez the idValidez to set
     */
    public void setIdValidez(Long idValidez) {
        this.idValidez = idValidez;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the validez
     */
    public String getValidez() {
        return validez;
    }

    /**
     * @param validez the validez to set
     */
    public void setValidez(String validez) {
        this.validez = validez;
    }

}
