package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class PuntosRedimidos implements Serializable {

    private Long idPuntosredimidos;
    private String cedula;
    private Long redimidos;
    private Date fecha;
    private Long pago;
    private String canal;

    public PuntosRedimidos() {
    }

    /**
     * @return the idPuntosredimidos
     */
    public Long getIdPuntosredimidos() {
        return idPuntosredimidos;
    }

    /**
     * @param idPuntosredimidos the idPuntosredimidos to set
     */
    public void setIdPuntosredimidos(Long idPuntosredimidos) {
        this.idPuntosredimidos = idPuntosredimidos;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the redimidos
     */
    public Long getRedimidos() {
        return redimidos;
    }

    /**
     * @param redimidos the redimidos to set
     */
    public void setRedimidos(Long redimidos) {
        this.redimidos = redimidos;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the pago
     */
    public Long getPago() {
        return pago;
    }

    /**
     * @param pago the pago to set
     */
    public void setPago(Long pago) {
        this.pago = pago;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }
    
    

}
