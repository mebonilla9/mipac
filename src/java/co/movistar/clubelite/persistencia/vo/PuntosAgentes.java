package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class PuntosAgentes implements Serializable {

    private String idPunto;
    private String cedula;
    private String nombrePunto;
    private String direccion;
    private String canal;
    private String ciudad;

    public PuntosAgentes() {
    }

    /**
     * @return the idPunto
     */
    public String getIdPunto() {
        return idPunto;
    }

    /**
     * @param idPunto the idPunto to set
     */
    public void setIdPunto(String idPunto) {
        this.idPunto = idPunto;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombrePunto
     */
    public String getNombrePunto() {
        return nombrePunto;
    }

    /**
     * @param nombrePunto the nombrePunto to set
     */
    public void setNombrePunto(String nombrePunto) {
        this.nombrePunto = nombrePunto;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

}
