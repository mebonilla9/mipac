package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class Meses implements Serializable {

    private Long idMes;
    private Long mes;
    private String tipo;

    public Meses() {
    }

    /**
     * @return the idMes
     */
    public Long getIdMes() {
        return idMes;
    }

    /**
     * @param idMes the idMes to set
     */
    public void setIdMes(Long idMes) {
        this.idMes = idMes;
    }

    /**
     * @return the mes
     */
    public Long getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(Long mes) {
        this.mes = mes;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
