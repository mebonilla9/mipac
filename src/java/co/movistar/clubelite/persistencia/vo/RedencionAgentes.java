package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class RedencionAgentes implements Serializable {

    private Long idRedencion;
    private String cedula;
    private String tipo;
    private String recibe;
    private String punto;
    private String kam;
    private Date fecha;
    private Long puntos;
    private Date fechaEntrega;
    private String autorizacion;
    private Long valor;
    private String telefono;
    private String canal;

    public RedencionAgentes() {
    }

    /**
     * @return the idRedencion
     */
    public Long getIdRedencion() {
        return idRedencion;
    }

    /**
     * @param idRedencion the idRedencion to set
     */
    public void setIdRedencion(Long idRedencion) {
        this.idRedencion = idRedencion;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the recibe
     */
    public String getRecibe() {
        return recibe;
    }

    /**
     * @param recibe the recibe to set
     */
    public void setRecibe(String recibe) {
        this.recibe = recibe;
    }

    /**
     * @return the punto
     */
    public String getPunto() {
        return punto;
    }

    /**
     * @param punto the punto to set
     */
    public void setPunto(String punto) {
        this.punto = punto;
    }

    /**
     * @return the kam
     */
    public String getKam() {
        return kam;
    }

    /**
     * @param kam the kam to set
     */
    public void setKam(String kam) {
        this.kam = kam;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the puntos
     */
    public Long getPuntos() {
        return puntos;
    }

    /**
     * @param puntos the puntos to set
     */
    public void setPuntos(Long puntos) {
        this.puntos = puntos;
    }

    /**
     * @return the fechaEntrega
     */
    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    /**
     * @param fechaEntrega the fechaEntrega to set
     */
    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    /**
     * @return the autorizacion
     */
    public String getAutorizacion() {
        return autorizacion;
    }

    /**
     * @param autorizacion the autorizacion to set
     */
    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }

    /**
     * @return the valor
     */
    public Long getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(Long valor) {
        this.valor = valor;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }
    
    
}
