package co.movistar.clubelite.persistencia.vo;

import java.io.Serializable;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class VwHistoriapuntos implements Serializable {

    private String cedula;
    private String canal;
    private Long mes;
    private Long redimidos;
    private Long obtenidos;

    public VwHistoriapuntos() {
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @param canal the canal to set
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * @return the mes
     */
    public Long getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(Long mes) {
        this.mes = mes;
    }

    /**
     * @return the redimidos
     */
    public Long getRedimidos() {
        return redimidos;
    }

    /**
     * @param redimidos the redimidos to set
     */
    public void setRedimidos(Long redimidos) {
        this.redimidos = redimidos;
    }

    /**
     * @return the obtenidos
     */
    public Long getObtenidos() {
        return obtenidos;
    }

    /**
     * @param obtenidos the obtenidos to set
     */
    public void setObtenidos(Long obtenidos) {
        this.obtenidos = obtenidos;
    }
}
