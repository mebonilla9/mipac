/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.persistencia.conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import co.movistar.clubelite.negocio.constantes.EMensajes;
import co.movistar.clubelite.negocio.excepciones.ClubEliteException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Manuel Ernesto Bonilla Muñoz
 */
public class ConexionBD {

    public static Connection conectar() throws ClubEliteException {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //Connection cnn = DriverManager.getConnection("jdbc:sqlserver://localhost;database=club_elite", "sa", "Rinnegan1988!");
            Context ctx = new InitialContext();
            DataSource ds = (DataSource)ctx.lookup("java:/jdbc/ClubEliteDS");
            Connection cnn = ds.getConnection();
            cnn.setAutoCommit(false);
            return cnn;
        } catch (NamingException | SQLException | ClassNotFoundException ex) {
            throw new ClubEliteException(EMensajes.ERROR_CONEXION_BD);
        }
    }

    public static void desconectar(Connection cnn) {
        desconectar(cnn, null);
    }

    public static void desconectar(PreparedStatement ps) {
        desconectar(null, ps);

    }

    public static void desconectar(Connection cnn, PreparedStatement ps) {
        try {
            if (ps != null) {
                ps.close();
            }
            if (cnn != null) {
                cnn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void rollback(Connection cnn) {
        try {
            cnn.rollback();
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
