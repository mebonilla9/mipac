package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import java.sql.Connection;
import co.movistar.clubelite.persistencia.dao.crud.RedencionAgentesCRUD;
import static co.movistar.clubelite.persistencia.dao.crud.RedencionAgentesCRUD.getRedencionAgentes;
import co.movistar.clubelite.persistencia.vo.RedencionAgentes;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RedencionAgentesDAO extends RedencionAgentesCRUD {

    public RedencionAgentesDAO(Connection cnn) {
        super(cnn);
    }

    public List<RedencionAgentes> consultarMisRedenciones(String nombre) throws SQLException {
        PreparedStatement sentencia = null;
        List<RedencionAgentes> lista = new ArrayList<>();
        try {

            String sql = "select * from Redencion_agentes where Kam like ? and Autorizacion is null";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, nombre);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getRedencionAgentes(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;
    }
}
