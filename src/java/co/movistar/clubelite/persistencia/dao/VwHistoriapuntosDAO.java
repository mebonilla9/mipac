package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.dao.crud.VwHistoriapuntosCRUD;
import java.sql.Connection;

public class VwHistoriapuntosDAO extends VwHistoriapuntosCRUD {

    public VwHistoriapuntosDAO(Connection cnn) {
        super(cnn);
    }
}
