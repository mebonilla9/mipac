package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import java.sql.Connection;
import co.movistar.clubelite.persistencia.dao.crud.UsuariosCRUD;
import static co.movistar.clubelite.persistencia.dao.crud.UsuariosCRUD.getUsuarios;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuariosDAO extends UsuariosCRUD {

    public UsuariosDAO(Connection cnn) {
        super(cnn);
    }

    public Usuarios consultar(String cedula, String contrasena) throws SQLException {
        PreparedStatement sentencia = null;
        Usuarios usuario = new Usuarios();
        try {
            String sql = "SELECT * FROM Usuarios WHERE Cedula = ? AND Contrasena = ?;";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, cedula);
            sentencia.setString(2, contrasena);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                usuario = getUsuarios(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return usuario;
    }

    public void reiniciarContrasena(Usuarios Usuarios) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Usuarios set Contrasena=? where Id_usuarios=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, Usuarios.getContrasena());
            sentencia.setObject(i++, Usuarios.getIdUsuarios());
            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }
    
    public List<Usuarios> consultarKams() throws SQLException{
        PreparedStatement sentencia = null;
        List<Usuarios> lista = new ArrayList<>();
        try {

            String sql = "select * from Usuarios where Tipo like 'k'";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getUsuarios(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;
    }
}
