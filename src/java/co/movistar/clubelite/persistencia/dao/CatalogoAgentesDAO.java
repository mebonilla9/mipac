package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.crud.CatalogoAgentesCRUD;
import co.movistar.clubelite.persistencia.vo.CatalogoAgentes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CatalogoAgentesDAO extends CatalogoAgentesCRUD {

    public CatalogoAgentesDAO(Connection cnn) {
        super(cnn);
    }

    public List<CatalogoAgentes> catalogoDisponibles(Long puntos, String canal) throws SQLException {
        PreparedStatement sentencia = null;
        List<CatalogoAgentes> lista = new ArrayList<>();
        try {
            String sql = "select * from Catalogo_agentes where PUNTOS <= ? AND UPPER(CANAL) = UPPER(?)";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, puntos);
            sentencia.setString(2, canal);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getCatalogoAgentes(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;
    }
}
