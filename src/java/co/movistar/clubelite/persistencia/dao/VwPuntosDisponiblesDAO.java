package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import static co.movistar.clubelite.persistencia.dao.crud.RedencionAgentesCRUD.getRedencionAgentes;
import co.movistar.clubelite.persistencia.dao.crud.VwPuntosDisponiblesCRUD;
import static co.movistar.clubelite.persistencia.dao.crud.VwPuntosDisponiblesCRUD.getVwPuntosDisponibles;
import co.movistar.clubelite.persistencia.vo.VwPuntosDisponibles;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VwPuntosDisponiblesDAO extends VwPuntosDisponiblesCRUD {

    public VwPuntosDisponiblesDAO(Connection cnn) {
        super(cnn);
    }
    
    public List<VwPuntosDisponibles> consultar(String cedula) throws SQLException {
        PreparedStatement sentencia = null;
        List<VwPuntosDisponibles> lista = new ArrayList<>();
        try {
            String sql = "select * from vwPuntos_disponibles where Cedula = ?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, cedula);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVwPuntosDisponibles(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }
    
    public VwPuntosDisponibles consultarPorCanal(String cedula, String canal) throws SQLException {
        PreparedStatement sentencia = null;
        VwPuntosDisponibles obj = null;
        try {
            String sql = "select * from vwPuntos_disponibles where Cedula = ? AND canal = ?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, cedula);
            sentencia.setString(2, canal);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getVwPuntosDisponibles(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;

    }
}
