package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.Meses;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MesesCRUD implements IGenericoDAO<Meses> {

    protected final int ID = 1;
    protected Connection cnn;

    public MesesCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Meses Meses) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Meses(Mes,Tipo) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, Meses.getMes());
            sentencia.setObject(i++, Meses.getTipo());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                Meses.setIdMes(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Meses Meses) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Meses set Mes=?,Tipo=? where id_mes=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, Meses.getMes());
            sentencia.setObject(i++, Meses.getTipo());
            sentencia.setObject(i++, Meses.getIdMes());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<Meses> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Meses> lista = new ArrayList<>();
        try {

            String sql = "select * from Meses";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getMeses(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Meses consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Meses obj = null;
        try {

            String sql = "select * from Meses where id_mes=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getMeses(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static Meses getMeses(ResultSet rs) throws SQLException {
        Meses Meses = new Meses();
        Meses.setIdMes(rs.getLong("id_mes"));
        Meses.setMes(rs.getLong("Mes"));
        Meses.setTipo(rs.getString("Tipo"));

        return Meses;
    }

    public static Meses getMeses(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        Meses Meses = new Meses();
        Integer columna = columnas.get("Meses_id_mes");
        if (columna != null) {
            Meses.setIdMes(rs.getLong(columna));
        }
        columna = columnas.get("Meses_Mes");
        if (columna != null) {
            Meses.setMes(rs.getLong(columna));
        }
        columna = columnas.get("Meses_Tipo");
        if (columna != null) {
            Meses.setTipo(rs.getString(columna));
        }
        return Meses;
    }

}
