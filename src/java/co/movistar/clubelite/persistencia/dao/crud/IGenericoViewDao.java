/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.clubelite.persistencia.dao.crud;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Lord_Nightmare
 */
public interface IGenericoViewDao<T> {
    
    public List<T> consultar() throws SQLException;
    
}
