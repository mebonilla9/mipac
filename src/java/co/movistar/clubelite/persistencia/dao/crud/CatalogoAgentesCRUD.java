package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.CatalogoAgentes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CatalogoAgentesCRUD implements IGenericoDAO<CatalogoAgentes> {

    protected final int ID = 1;
    protected Connection cnn;

    public CatalogoAgentesCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(CatalogoAgentes CatalogoAgentes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Catalogo_agentes(DESCRIPCION,PUNTOS,canal) values (?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, CatalogoAgentes.getDescripcion());
            sentencia.setObject(i++, CatalogoAgentes.getPuntos());
            sentencia.setObject(i++, CatalogoAgentes.getCanal());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                CatalogoAgentes.setTipo(rs.getString(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(CatalogoAgentes CatalogoAgentes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Catalogo_agentes set DESCRIPCION=?,PUNTOS=?,canal=? where Tipo=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, CatalogoAgentes.getDescripcion());
            sentencia.setObject(i++, CatalogoAgentes.getPuntos());
            sentencia.setObject(i++, CatalogoAgentes.getCanal());
            sentencia.setObject(i++, CatalogoAgentes.getTipo());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<CatalogoAgentes> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<CatalogoAgentes> lista = new ArrayList<>();
        try {

            String sql = "select * from Catalogo_agentes";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getCatalogoAgentes(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public CatalogoAgentes consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        CatalogoAgentes obj = null;
        try {

            String sql = "select * from Catalogo_agentes where Tipo=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getCatalogoAgentes(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static CatalogoAgentes getCatalogoAgentes(ResultSet rs) throws SQLException {
        CatalogoAgentes CatalogoAgentes = new CatalogoAgentes();
        CatalogoAgentes.setTipo(rs.getString("Tipo"));
        CatalogoAgentes.setDescripcion(rs.getString("DESCRIPCION"));
        CatalogoAgentes.setPuntos(rs.getLong("PUNTOS"));
        CatalogoAgentes.setCanal(rs.getString("canal"));

        return CatalogoAgentes;
    }

    public static CatalogoAgentes getCatalogoAgentes(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        CatalogoAgentes CatalogoAgentes = new CatalogoAgentes();
        Integer columna = columnas.get("Catalogo_agentes_Tipo");
        if (columna != null) {
            CatalogoAgentes.setTipo(rs.getString(columna));
        }
        columna = columnas.get("Catalogo_agentes_DESCRIPCION");
        if (columna != null) {
            CatalogoAgentes.setDescripcion(rs.getString(columna));
        }
        columna = columnas.get("Catalogo_agentes_PUNTOS");
        if (columna != null) {
            CatalogoAgentes.setPuntos(rs.getLong(columna));
        }
        columna = columnas.get("Catalogo_agentes_canal");
        if (columna != null) {
            CatalogoAgentes.setCanal(rs.getString(columna));
        }
        return CatalogoAgentes;
    }

}
