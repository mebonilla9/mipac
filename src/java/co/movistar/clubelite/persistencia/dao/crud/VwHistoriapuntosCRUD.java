package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.VwHistoriapuntos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class VwHistoriapuntosCRUD implements IGenericoViewDao<VwHistoriapuntos> {

    protected final int ID = 1;
    protected Connection cnn;

    public VwHistoriapuntosCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public List<VwHistoriapuntos> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<VwHistoriapuntos> lista = new ArrayList<>();
        try {

            String sql = "select * from vwhistoriapuntos";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVwhistoriapuntos(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    public static VwHistoriapuntos getVwhistoriapuntos(ResultSet rs) throws SQLException {
        VwHistoriapuntos vwhistoriapuntos = new VwHistoriapuntos();
        vwhistoriapuntos.setCedula(rs.getString("cedula"));
        vwhistoriapuntos.setCanal(rs.getString("canal"));
        vwhistoriapuntos.setMes(rs.getLong("mes"));
        vwhistoriapuntos.setRedimidos(rs.getLong("Redimidos"));
        vwhistoriapuntos.setObtenidos(rs.getLong("Obtenidos"));

        return vwhistoriapuntos;
    }

    public static VwHistoriapuntos getVwhistoriapuntos(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        VwHistoriapuntos vwhistoriapuntos = new VwHistoriapuntos();
        Integer columna = columnas.get("vwhistoriapuntos_cedula");
        if (columna != null) {
            vwhistoriapuntos.setCedula(rs.getString(columna));
        }
        columna = columnas.get("vwhistoriapuntos_canal");
        if (columna != null) {
            vwhistoriapuntos.setCanal(rs.getString(columna));
        }
        columna = columnas.get("vwhistoriapuntos_mes");
        if (columna != null) {
            vwhistoriapuntos.setMes(rs.getLong(columna));
        }
        columna = columnas.get("vwhistoriapuntos_Redimidos");
        if (columna != null) {
            vwhistoriapuntos.setRedimidos(rs.getLong(columna));
        }
        columna = columnas.get("vwhistoriapuntos_Obtenidos");
        if (columna != null) {
            vwhistoriapuntos.setObtenidos(rs.getLong(columna));
        }
        return vwhistoriapuntos;
    }

}
