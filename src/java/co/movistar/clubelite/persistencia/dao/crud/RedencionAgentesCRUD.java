package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.negocio.util.DateUtil;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.RedencionAgentes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RedencionAgentesCRUD implements IGenericoDAO<RedencionAgentes> {

    protected final int ID = 1;
    protected Connection cnn;

    public RedencionAgentesCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(RedencionAgentes RedencionAgentes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Redencion_agentes(Cedula,Tipo,Recibe,Punto,Kam,Fecha,Puntos,Fecha_entrega,Autorizacion,Valor,telefono,canal) values (?,?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, RedencionAgentes.getCedula());
            sentencia.setObject(i++, RedencionAgentes.getTipo());
            sentencia.setObject(i++, RedencionAgentes.getRecibe());
            sentencia.setObject(i++, RedencionAgentes.getPunto());
            sentencia.setObject(i++, RedencionAgentes.getKam());
            sentencia.setObject(i++, DateUtil.parseTimestamp(RedencionAgentes.getFecha()));
            sentencia.setObject(i++, RedencionAgentes.getPuntos());
            sentencia.setObject(i++, DateUtil.parseTimestamp(RedencionAgentes.getFechaEntrega()));
            sentencia.setObject(i++, RedencionAgentes.getAutorizacion());
            sentencia.setObject(i++, RedencionAgentes.getValor());
            sentencia.setObject(i++, RedencionAgentes.getTelefono());
            sentencia.setObject(i++, RedencionAgentes.getCanal());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                RedencionAgentes.setIdRedencion(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(RedencionAgentes RedencionAgentes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Redencion_agentes set Cedula=?,Tipo=?,Recibe=?,Punto=?,Kam=?,Fecha=?,Puntos=?,Fecha_entrega=?,Autorizacion=?,Valor=?,telefono=?,canal=? where Id_redencion=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, RedencionAgentes.getCedula());
            sentencia.setObject(i++, RedencionAgentes.getTipo());
            sentencia.setObject(i++, RedencionAgentes.getRecibe());
            sentencia.setObject(i++, RedencionAgentes.getPunto());
            sentencia.setObject(i++, RedencionAgentes.getKam());
            sentencia.setObject(i++, DateUtil.parseTimestamp(RedencionAgentes.getFecha()));
            sentencia.setObject(i++, RedencionAgentes.getPuntos());
            sentencia.setObject(i++, DateUtil.parseTimestamp(RedencionAgentes.getFechaEntrega()));
            sentencia.setObject(i++, RedencionAgentes.getAutorizacion());
            sentencia.setObject(i++, RedencionAgentes.getValor());
            sentencia.setObject(i++, RedencionAgentes.getTelefono());
            sentencia.setObject(i++, RedencionAgentes.getCanal());
            sentencia.setObject(i++, RedencionAgentes.getIdRedencion());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<RedencionAgentes> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<RedencionAgentes> lista = new ArrayList<>();
        try {

            String sql = "select * from Redencion_agentes";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getRedencionAgentes(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public RedencionAgentes consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        RedencionAgentes obj = null;
        try {

            String sql = "select * from Redencion_agentes where Id_redencion=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getRedencionAgentes(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static RedencionAgentes getRedencionAgentes(ResultSet rs) throws SQLException {
        RedencionAgentes RedencionAgentes = new RedencionAgentes();
        RedencionAgentes.setIdRedencion(rs.getLong("Id_redencion"));
        RedencionAgentes.setCedula(rs.getString("Cedula"));
        RedencionAgentes.setTipo(rs.getString("Tipo"));
        RedencionAgentes.setRecibe(rs.getString("Recibe"));
        RedencionAgentes.setPunto(rs.getString("Punto"));
        RedencionAgentes.setKam(rs.getString("Kam"));
        RedencionAgentes.setFecha(rs.getTimestamp("Fecha"));
        RedencionAgentes.setPuntos(rs.getLong("Puntos"));
        RedencionAgentes.setFechaEntrega(rs.getTimestamp("Fecha_entrega"));
        RedencionAgentes.setAutorizacion(rs.getString("Autorizacion"));
        RedencionAgentes.setValor(rs.getLong("Valor"));
        RedencionAgentes.setTelefono(rs.getString("telefono"));
        RedencionAgentes.setCanal(rs.getString("canal"));

        return RedencionAgentes;
    }

    public static RedencionAgentes getRedencionAgentes(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        RedencionAgentes RedencionAgentes = new RedencionAgentes();
        Integer columna = columnas.get("Redencion_agentes_Id_redencion");
        if (columna != null) {
            RedencionAgentes.setIdRedencion(rs.getLong(columna));
        }
        columna = columnas.get("Redencion_agentes_Cedula");
        if (columna != null) {
            RedencionAgentes.setCedula(rs.getString(columna));
        }
        columna = columnas.get("Redencion_agentes_Tipo");
        if (columna != null) {
            RedencionAgentes.setTipo(rs.getString(columna));
        }
        columna = columnas.get("Redencion_agentes_Recibe");
        if (columna != null) {
            RedencionAgentes.setRecibe(rs.getString(columna));
        }
        columna = columnas.get("Redencion_agentes_Punto");
        if (columna != null) {
            RedencionAgentes.setPunto(rs.getString(columna));
        }
        columna = columnas.get("Redencion_agentes_Kam");
        if (columna != null) {
            RedencionAgentes.setKam(rs.getString(columna));
        }
        columna = columnas.get("Redencion_agentes_Fecha");
        if (columna != null) {
            RedencionAgentes.setFecha(rs.getTimestamp(columna));
        }
        columna = columnas.get("Redencion_agentes_Puntos");
        if (columna != null) {
            RedencionAgentes.setPuntos(rs.getLong(columna));
        }
        columna = columnas.get("Redencion_agentes_Fecha_entrega");
        if (columna != null) {
            RedencionAgentes.setFechaEntrega(rs.getTimestamp(columna));
        }
        columna = columnas.get("Redencion_agentes_Autorizacion");
        if (columna != null) {
            RedencionAgentes.setAutorizacion(rs.getString(columna));
        }
        columna = columnas.get("Redencion_agentes_Valor");
        if (columna != null) {
            RedencionAgentes.setValor(rs.getLong(columna));
        }
        columna = columnas.get("Redencion_agentes_telefono");
        if (columna != null) {
            RedencionAgentes.setTelefono(rs.getString(columna));
        }
        columna = columnas.get("Redencion_agentes_canal");
        if (columna != null) {
            RedencionAgentes.setCanal(rs.getString(columna));
        }
        return RedencionAgentes;
    }

}
