package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.VwPuntosDisponibles;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class VwPuntosDisponiblesCRUD implements IGenericoViewDao<VwPuntosDisponibles> {

    protected final int ID = 1;
    protected Connection cnn;

    public VwPuntosDisponiblesCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public List<VwPuntosDisponibles> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<VwPuntosDisponibles> lista = new ArrayList<>();
        try {

            String sql = "select * from vwPuntos_disponibles";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVwPuntosDisponibles(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    public static VwPuntosDisponibles getVwPuntosDisponibles(ResultSet rs) throws SQLException {
        VwPuntosDisponibles vwPuntosDisponibles = new VwPuntosDisponibles();
        vwPuntosDisponibles.setCedula(rs.getString("Cedula"));
        vwPuntosDisponibles.setCanal(rs.getString("canal"));
        vwPuntosDisponibles.setDisponibles(rs.getLong("disponibles"));

        return vwPuntosDisponibles;
    }

    public static VwPuntosDisponibles getVwPuntosDisponibles(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        VwPuntosDisponibles vwPuntosDisponibles = new VwPuntosDisponibles();
        Integer columna = columnas.get("vwPuntos_disponibles_Cedula");
        if (columna != null) {
            vwPuntosDisponibles.setCedula(rs.getString(columna));
        }
        columna = columnas.get("vwPuntos_disponibles_canal");
        if (columna != null) {
            vwPuntosDisponibles.setCanal(rs.getString(columna));
        }
        columna = columnas.get("vwPuntos_disponibles_disponibles");
        if (columna != null) {
            vwPuntosDisponibles.setDisponibles(rs.getLong(columna));
        }
        return vwPuntosDisponibles;
    }

}
