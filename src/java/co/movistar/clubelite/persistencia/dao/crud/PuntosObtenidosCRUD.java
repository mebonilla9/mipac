package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.PuntosObtenidos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PuntosObtenidosCRUD implements IGenericoDAO<PuntosObtenidos> {

    protected final int ID = 1;
    protected Connection cnn;

    public PuntosObtenidosCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(PuntosObtenidos PuntosObtenidos) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Puntos_obtenidos(Cedula,Puntos,Mes,Canal) values (?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, PuntosObtenidos.getCedula());
            sentencia.setObject(i++, PuntosObtenidos.getPuntos());
            sentencia.setObject(i++, PuntosObtenidos.getMes());
            sentencia.setObject(i++, PuntosObtenidos.getCanal());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                PuntosObtenidos.setIdPuntosobtenidos(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(PuntosObtenidos PuntosObtenidos) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Puntos_obtenidos set Cedula=?,Puntos=?,Mes=?,Canal=? where Id_puntosobtenidos=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, PuntosObtenidos.getCedula());
            sentencia.setObject(i++, PuntosObtenidos.getPuntos());
            sentencia.setObject(i++, PuntosObtenidos.getMes());
            sentencia.setObject(i++, PuntosObtenidos.getCanal());
            sentencia.setObject(i++, PuntosObtenidos.getIdPuntosobtenidos());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<PuntosObtenidos> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<PuntosObtenidos> lista = new ArrayList<>();
        try {

            String sql = "select * from Puntos_obtenidos";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getPuntosObtenidos(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public PuntosObtenidos consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        PuntosObtenidos obj = null;
        try {

            String sql = "select * from Puntos_obtenidos where Id_puntosobtenidos=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getPuntosObtenidos(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static PuntosObtenidos getPuntosObtenidos(ResultSet rs) throws SQLException {
        PuntosObtenidos PuntosObtenidos = new PuntosObtenidos();
        PuntosObtenidos.setIdPuntosobtenidos(rs.getLong("Id_puntosobtenidos"));
        PuntosObtenidos.setCedula(rs.getString("Cedula"));
        PuntosObtenidos.setPuntos(rs.getLong("Puntos"));
        PuntosObtenidos.setMes(rs.getString("Mes"));
        PuntosObtenidos.setCanal(rs.getString("Canal"));

        return PuntosObtenidos;
    }

    public static PuntosObtenidos getPuntosObtenidos(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        PuntosObtenidos PuntosObtenidos = new PuntosObtenidos();
        Integer columna = columnas.get("Puntos_obtenidos_Id_puntosobtenidos");
        if (columna != null) {
            PuntosObtenidos.setIdPuntosobtenidos(rs.getLong(columna));
        }
        columna = columnas.get("Puntos_obtenidos_Cedula");
        if (columna != null) {
            PuntosObtenidos.setCedula(rs.getString(columna));
        }
        columna = columnas.get("Puntos_obtenidos_Puntos");
        if (columna != null) {
            PuntosObtenidos.setPuntos(rs.getLong(columna));
        }
        columna = columnas.get("Puntos_obtenidos_Mes");
        if (columna != null) {
            PuntosObtenidos.setMes(rs.getString(columna));
        }
        columna = columnas.get("Puntos_obtenidos_Canal");
        if (columna != null) {
            PuntosObtenidos.setCanal(rs.getString(columna));
        }
        return PuntosObtenidos;
    }

}
