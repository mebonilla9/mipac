package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.PuntosAgentes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PuntosAgentesCRUD implements IGenericoDAO<PuntosAgentes> {

    protected final int ID = 1;
    protected Connection cnn;

    public PuntosAgentesCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(PuntosAgentes PuntosAgentes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Puntos_agentes(Cedula,Nombre_punto,Direccion,canal,ciudad) values (?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, PuntosAgentes.getCedula());
            sentencia.setObject(i++, PuntosAgentes.getNombrePunto());
            sentencia.setObject(i++, PuntosAgentes.getDireccion());
            sentencia.setObject(i++, PuntosAgentes.getCanal());
            sentencia.setObject(i++, PuntosAgentes.getCiudad());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                PuntosAgentes.setIdPunto(rs.getString(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(PuntosAgentes PuntosAgentes) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Puntos_agentes set Cedula=?,Nombre_punto=?,Direccion=?,canal=?,ciudad=? where id_punto=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, PuntosAgentes.getCedula());
            sentencia.setObject(i++, PuntosAgentes.getNombrePunto());
            sentencia.setObject(i++, PuntosAgentes.getDireccion());
            sentencia.setObject(i++, PuntosAgentes.getCanal());
            sentencia.setObject(i++, PuntosAgentes.getCiudad());
            sentencia.setObject(i++, PuntosAgentes.getIdPunto());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<PuntosAgentes> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<PuntosAgentes> lista = new ArrayList<>();
        try {

            String sql = "select * from Puntos_agentes";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getPuntosAgentes(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public PuntosAgentes consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        PuntosAgentes obj = null;
        try {

            String sql = "select * from Puntos_agentes where id_punto=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getPuntosAgentes(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static PuntosAgentes getPuntosAgentes(ResultSet rs) throws SQLException {
        PuntosAgentes PuntosAgentes = new PuntosAgentes();
        PuntosAgentes.setIdPunto(rs.getString("id_punto"));
        PuntosAgentes.setCedula(rs.getString("Cedula"));
        PuntosAgentes.setNombrePunto(rs.getString("Nombre_punto"));
        PuntosAgentes.setDireccion(rs.getString("Direccion"));
        PuntosAgentes.setCanal(rs.getString("canal"));
        PuntosAgentes.setCiudad(rs.getString("ciudad"));

        return PuntosAgentes;
    }

    public static PuntosAgentes getPuntosAgentes(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        PuntosAgentes PuntosAgentes = new PuntosAgentes();
        Integer columna = columnas.get("Puntos_agentes_id_punto");
        if (columna != null) {
            PuntosAgentes.setIdPunto(rs.getString(columna));
        }
        columna = columnas.get("Puntos_agentes_Cedula");
        if (columna != null) {
            PuntosAgentes.setCedula(rs.getString(columna));
        }
        columna = columnas.get("Puntos_agentes_Nombre_punto");
        if (columna != null) {
            PuntosAgentes.setNombrePunto(rs.getString(columna));
        }
        columna = columnas.get("Puntos_agentes_Direccion");
        if (columna != null) {
            PuntosAgentes.setDireccion(rs.getString(columna));
        }
        columna = columnas.get("Puntos_agentes_canal");
        if (columna != null) {
            PuntosAgentes.setCanal(rs.getString(columna));
        }
        columna = columnas.get("Puntos_agentes_ciudad");
        if (columna != null) {
            PuntosAgentes.setCiudad(rs.getString(columna));
        }
        return PuntosAgentes;
    }

}
