package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.negocio.util.DateUtil;
import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.PuntosRedimidos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PuntosRedimidosCRUD implements IGenericoDAO<PuntosRedimidos> {

    protected final int ID = 1;
    protected Connection cnn;

    public PuntosRedimidosCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(PuntosRedimidos PuntosRedimidos) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Puntos_Redimidos(Cedula,Redimidos,Fecha,pago,canal) values (?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, PuntosRedimidos.getCedula());
            sentencia.setObject(i++, PuntosRedimidos.getRedimidos());
            sentencia.setObject(i++, DateUtil.parseTimestamp(PuntosRedimidos.getFecha()));
            sentencia.setObject(i++, PuntosRedimidos.getPago());
            sentencia.setObject(i++, PuntosRedimidos.getCanal());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                PuntosRedimidos.setIdPuntosredimidos(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(PuntosRedimidos PuntosRedimidos) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Puntos_Redimidos set Cedula=?,Redimidos=?,Fecha=?,pago=?,canal=? where Id_puntosredimidos=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, PuntosRedimidos.getCedula());
            sentencia.setObject(i++, PuntosRedimidos.getRedimidos());
            sentencia.setObject(i++, DateUtil.parseTimestamp(PuntosRedimidos.getFecha()));
            sentencia.setObject(i++, PuntosRedimidos.getPago());
            sentencia.setObject(i++, PuntosRedimidos.getCanal());
            sentencia.setObject(i++, PuntosRedimidos.getIdPuntosredimidos());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<PuntosRedimidos> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<PuntosRedimidos> lista = new ArrayList<>();
        try {

            String sql = "select * from Puntos_Redimidos";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getPuntosRedimidos(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public PuntosRedimidos consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        PuntosRedimidos obj = null;
        try {

            String sql = "select * from Puntos_Redimidos where Id_puntosredimidos=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getPuntosRedimidos(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static PuntosRedimidos getPuntosRedimidos(ResultSet rs) throws SQLException {
        PuntosRedimidos PuntosRedimidos = new PuntosRedimidos();
        PuntosRedimidos.setIdPuntosredimidos(rs.getLong("Id_puntosredimidos"));
        PuntosRedimidos.setCedula(rs.getString("Cedula"));
        PuntosRedimidos.setRedimidos(rs.getLong("Redimidos"));
        PuntosRedimidos.setFecha(rs.getTimestamp("Fecha"));
        PuntosRedimidos.setPago(rs.getLong("pago"));
        PuntosRedimidos.setCanal(rs.getString("canal"));

        return PuntosRedimidos;
    }

    public static PuntosRedimidos getPuntosRedimidos(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        PuntosRedimidos PuntosRedimidos = new PuntosRedimidos();
        Integer columna = columnas.get("Puntos_Redimidos_Id_puntosredimidos");
        if (columna != null) {
            PuntosRedimidos.setIdPuntosredimidos(rs.getLong(columna));
        }
        columna = columnas.get("Puntos_Redimidos_Cedula");
        if (columna != null) {
            PuntosRedimidos.setCedula(rs.getString(columna));
        }
        columna = columnas.get("Puntos_Redimidos_Redimidos");
        if (columna != null) {
            PuntosRedimidos.setRedimidos(rs.getLong(columna));
        }
        columna = columnas.get("Puntos_Redimidos_Fecha");
        if (columna != null) {
            PuntosRedimidos.setFecha(rs.getTimestamp(columna));
        }
        columna = columnas.get("Puntos_Redimidos_pago");
        if (columna != null) {
            PuntosRedimidos.setPago(rs.getLong(columna));
        }
        columna = columnas.get("Puntos_Redimidos_canal");
        if (columna != null) {
            PuntosRedimidos.setCanal(rs.getString(columna));
        }
        return PuntosRedimidos;
    }

}
