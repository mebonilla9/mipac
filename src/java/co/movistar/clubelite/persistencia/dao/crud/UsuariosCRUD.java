package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.Usuarios;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UsuariosCRUD implements IGenericoDAO<Usuarios> {

    protected final int ID = 1;
    protected Connection cnn;

    public UsuariosCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Usuarios Usuarios) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Usuarios(Cedula,Nombre,Contrasena,Tipo,Email,Celular,canal) values (?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, Usuarios.getCedula());
            sentencia.setObject(i++, Usuarios.getNombre());
            sentencia.setObject(i++, Usuarios.getContrasena());
            sentencia.setObject(i++, Usuarios.getTipo());
            sentencia.setObject(i++, Usuarios.getEmail());
            sentencia.setObject(i++, Usuarios.getCelular());
            sentencia.setObject(i++, Usuarios.getCanal());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                Usuarios.setIdUsuarios(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Usuarios Usuarios) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Usuarios set Cedula=?,Nombre=?,Contrasena=?,Tipo=?,Email=?,Celular=?,canal=? where Id_usuarios=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, Usuarios.getCedula());
            sentencia.setObject(i++, Usuarios.getNombre());
            sentencia.setObject(i++, Usuarios.getContrasena());
            sentencia.setObject(i++, Usuarios.getTipo());
            sentencia.setObject(i++, Usuarios.getEmail());
            sentencia.setObject(i++, Usuarios.getCelular());
            sentencia.setObject(i++, Usuarios.getCanal());
            sentencia.setObject(i++, Usuarios.getIdUsuarios());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<Usuarios> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Usuarios> lista = new ArrayList<>();
        try {

            String sql = "select * from Usuarios";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getUsuarios(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }

    @Override
    public Usuarios consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Usuarios obj = null;
        try {

            String sql = "select * from Usuarios where Id_usuarios=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getUsuarios(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static Usuarios getUsuarios(ResultSet rs) throws SQLException {
        Usuarios Usuarios = new Usuarios();
        Usuarios.setIdUsuarios(rs.getLong("Id_usuarios"));
        Usuarios.setCedula(rs.getString("Cedula"));
        Usuarios.setNombre(rs.getString("Nombre"));
        Usuarios.setContrasena(rs.getString("Contrasena"));
        Usuarios.setTipo(rs.getString("Tipo"));
        Usuarios.setEmail(rs.getString("Email"));
        Usuarios.setCelular(rs.getString("Celular"));
        Usuarios.setCanal(rs.getString("canal"));

        return Usuarios;
    }

    public static Usuarios getUsuarios(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        Usuarios Usuarios = new Usuarios();
        Integer columna = columnas.get("Usuarios_Id_usuarios");
        if (columna != null) {
            Usuarios.setIdUsuarios(rs.getLong(columna));
        }
        columna = columnas.get("Usuarios_Cedula");
        if (columna != null) {
            Usuarios.setCedula(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Nombre");
        if (columna != null) {
            Usuarios.setNombre(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Contrasena");
        if (columna != null) {
            Usuarios.setContrasena(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Tipo");
        if (columna != null) {
            Usuarios.setTipo(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Email");
        if (columna != null) {
            Usuarios.setEmail(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_Celular");
        if (columna != null) {
            Usuarios.setCelular(rs.getString(columna));
        }
        columna = columnas.get("Usuarios_canal");
        if (columna != null) {
            Usuarios.setCanal(rs.getString(columna));
        }
        return Usuarios;
    }

}
