package co.movistar.clubelite.persistencia.dao.crud;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.vo.Validez;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ValidezCRUD implements IGenericoDAO<Validez> {

    protected final int ID = 1;
    protected Connection cnn;

    public ValidezCRUD(Connection cnn) {
        this.cnn = cnn;
    }

    @Override
    public void insertar(Validez Validez) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "insert into Validez(Tipo,Validez) values (?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++, Validez.getTipo());
            sentencia.setObject(i++, Validez.getValidez());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                Validez.setIdValidez(rs.getLong(ID));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public void editar(Validez Validez) throws SQLException {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "update Validez set Tipo=?,Validez=? where Id_validez=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, Validez.getTipo());
            sentencia.setObject(i++, Validez.getValidez());
            sentencia.setObject(i++, Validez.getIdValidez());

            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }
    }

    @Override
    public List<Validez> consultar() throws SQLException {
        PreparedStatement sentencia = null;
        List<Validez> lista = new ArrayList<>();
        try {

            String sql = "select * from Validez";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getValidez(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;
    }

    @Override
    public Validez consultar(Long id) throws SQLException {
        PreparedStatement sentencia = null;
        Validez obj = null;
        try {
            String sql = "select * from Validez where Id_validez=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getValidez(rs);
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return obj;
    }

    public static Validez getValidez(ResultSet rs) throws SQLException {
        Validez Validez = new Validez();
        Validez.setIdValidez(rs.getLong("Id_validez"));
        Validez.setTipo(rs.getString("Tipo"));
        Validez.setValidez(rs.getString("Validez"));

        return Validez;
    }

    public static Validez getValidez(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
        Validez Validez = new Validez();
        Integer columna = columnas.get("Validez_Id_validez");
        if (columna != null) {
            Validez.setIdValidez(rs.getLong(columna));
        }
        columna = columnas.get("Validez_Tipo");
        if (columna != null) {
            Validez.setTipo(rs.getString(columna));
        }
        columna = columnas.get("Validez_Validez");
        if (columna != null) {
            Validez.setValidez(rs.getString(columna));
        }
        return Validez;
    }

}
