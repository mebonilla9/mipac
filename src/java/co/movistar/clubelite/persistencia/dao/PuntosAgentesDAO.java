package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.crud.PuntosAgentesCRUD;
import static co.movistar.clubelite.persistencia.dao.crud.PuntosAgentesCRUD.getPuntosAgentes;
import co.movistar.clubelite.persistencia.vo.PuntosAgentes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PuntosAgentesDAO extends PuntosAgentesCRUD {
    
    public PuntosAgentesDAO(Connection cnn) {
        super(cnn);
    }
    
    public List<PuntosAgentes> consultar(String canal, String cedula) throws SQLException {
        PreparedStatement sentencia = null;
        List<PuntosAgentes> lista = new ArrayList<>();
        try {
            String sql = "select * from Puntos_agentes where canal = ? AND Cedula = ?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, canal);
            sentencia.setString(2, cedula);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getPuntosAgentes(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;
        
    }
}
