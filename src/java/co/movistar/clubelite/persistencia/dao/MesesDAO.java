package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.crud.MesesCRUD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MesesDAO extends MesesCRUD {

    public MesesDAO(Connection cnn) {
        super(cnn);
    }

    public void reiniciarMeses() throws SQLException {
        PreparedStatement sentencia = null;
        try {
            String sql = "delete from Meses";
            sentencia = cnn.prepareStatement(sql);
            sentencia.executeUpdate();
        } finally {
            ConexionBD.desconectar(sentencia);
        }

    }
}
