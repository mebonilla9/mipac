package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import co.movistar.clubelite.persistencia.dao.crud.PuntosObtenidosCRUD;
import static co.movistar.clubelite.persistencia.dao.crud.PuntosObtenidosCRUD.getPuntosObtenidos;
import co.movistar.clubelite.persistencia.vo.PuntosObtenidos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PuntosObtenidosDAO extends PuntosObtenidosCRUD {

    public PuntosObtenidosDAO(Connection cnn) {
        super(cnn);
    }
    
    public List<PuntosObtenidos> consultarCanalesObtenidos(String cedula) throws SQLException {
        PreparedStatement sentencia = null;
        List<PuntosObtenidos> lista = new ArrayList<>();
        try {
            String sql = "select distinct(Canal) from Puntos_obtenidos where Cedula like ?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setString(1, cedula);
            ResultSet rs = sentencia.executeQuery();
            Map<String, Integer> columnas = new HashMap<>();
            columnas.put("Puntos_obtenidos_Canal", 1);
            while (rs.next()) {
                lista.add(getPuntosObtenidos(rs,columnas));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;

    }
}
