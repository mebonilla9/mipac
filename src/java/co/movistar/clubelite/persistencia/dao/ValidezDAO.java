package co.movistar.clubelite.persistencia.dao;

import co.movistar.clubelite.persistencia.conexion.ConexionBD;
import java.sql.Connection;
import co.movistar.clubelite.persistencia.dao.crud.ValidezCRUD;
import static co.movistar.clubelite.persistencia.dao.crud.ValidezCRUD.getValidez;
import co.movistar.clubelite.persistencia.vo.Validez;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ValidezDAO extends ValidezCRUD {

    public ValidezDAO(Connection cnn) {
        super(cnn);
    }
    
    public List<Validez> consultarActivos() throws SQLException {
        PreparedStatement sentencia = null;
        List<Validez> lista = new ArrayList<>();
        try {

            String sql = "select * from Validez where Validez like 's'";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getValidez(rs));
            }
        } finally {
            ConexionBD.desconectar(sentencia);
        }
        return lista;
    }
}
