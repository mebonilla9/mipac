var thatMeses = null;
var mesesVista = {
    init: function () {
        thatMeses = this;
        $('select').material_select();
        $('#formMeses').on('submit', thatMeses.registrarMeses);
    },
    registrarMeses: function (e) {
        e.preventDefault();
        var obtenibles = _dom.obtenerValorSelect('#cboObtenibles');
        var redimibles = _dom.obtenerValorSelect('#cboRedimibles');
        for (var i = 0; i < obtenibles.length; i++) {
            mesesModelo.mesesRegistrar.push({mes: parseInt(obtenibles[i]), tipo: 'o'});
        }
        for (var i = 0; i < redimibles.length; i++) {
            mesesModelo.mesesRegistrar.push({mes: parseInt(redimibles[i]), tipo: 'r'});
        }
        mesesControl.registrarMeses(JSON.stringify(mesesModelo.mesesRegistrar), thatMeses.registrarMesesCallback);
    },
    registrarMesesCallback: function (data) {
        _dom.mostrarDialogo('Meses modificados correctamente');
    }
};
mesesVista.init();

