var thatRegistrar;
var registrarVista = {
    init:function(){
        console.log('registrar cargado');
        thatRegistrar = this;
        _dom.iniciarSelect($('#cboTipo'));
        _dom.iniciarSelect($('#cboCanal'));
        thatRegistrar.asignarEventos();
    },
    asignarEventos:function(){
        $('#cboTipo').on('change',thatRegistrar.registrarTipo);
        $('#cboCanal').on('change',thatRegistrar.registrarCanal);
        $('#formRegistrar').on('submit',thatRegistrar.registrarCliente);
    },
    registrarTipo:function(){
        registrarModelo.usuarioRegistrar.tipo =  _dom.obtenerValorSelect($('#cboTipo'));
    },
    registrarCanal:function(){
        registrarModelo.usuarioRegistrar.canal = _dom.obtenerValorSelect($('#cboCanal'));
    },
    registrarCliente:function(e){
        e.preventDefault();
        registrarModelo.usuarioRegistrar.nombre = $('#txtNombre').val();
        registrarModelo.usuarioRegistrar.cedula = $('#txtIdentificacion').val();
        
        var tipo = _dom.obtenerValorSelect('#cboTipo');
        if (tipo === "") {
            _dom.mostrarPanelError("Debe seleccionar un tipo de usuario valido");
            return;
        }
        
        var canal = _dom.obtenerValorSelect('#cboCanal');
        if (canal === "") {
            _dom.mostrarPanelError("Debe seleccionar un tipo de canal valido");
            return;
        }
        
        registrarControl.registrar(JSON.stringify(registrarModelo.usuarioRegistrar),thatRegistrar.callbackRegistrar);
    },
    callbackRegistrar:function(data){
        _dom.mostrarDialogo(data.mensaje);
    }
};
registrarVista.init();


