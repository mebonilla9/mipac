/* global _app, _rutas */

var agentesControl = {
    consultarCanalesUsuario: function (callback) {
        return _app.ajax({
            'url': _rutas.puntosObtenidos.canal,
            'completado': callback
        });
    },
    consultarPuntosDisponibles: function (data, callback) {
        return _app.ajax({
            'data': data,
            'url': _rutas.puntosDisponibles.puntosCanal,
            'completado': callback
        });
    },
    consultarPuntosAgentes: function (data, callback) {
        return _app.ajax({
            'data': data,
            'url': _rutas.puntosAgentes.canal,
            'completado': callback
        });
    },
    consultarProductosDisponibles: function (data, callback) {
        return _app.ajax({
            'data': data,
            'url': _rutas.catalogoAgentes.consultar,
            'completado': callback
        });
    },
    consultarKams: function (callback) {
        return _app.ajax({
            'url': _rutas.usuarios.kams,
            'completado': callback
        });
    },
    insertarRedencion: function (data, callback) {
        return _app.ajax({
            'data': data,
            'url': _rutas.kam.nuevo,
            'completado': callback
        });
    }
};