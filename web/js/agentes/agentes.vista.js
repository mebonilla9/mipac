/* global _dom, agentesControl */

var thatAgentes = null;
var agentesVista = {
    init: function () {
        thatAgentes = this;
        _dom.iniciarSelect('#cboCanales');
        _dom.iniciarSelect('#cboPremios');
        _dom.iniciarSelect('#cboPuntos');
        _dom.iniciarSelect('#cboKams');
        $('#formAgentes').on('submit', thatAgentes.redimirPremio);
        thatAgentes.consultarCanalesUsuario();
        thatAgentes.consultarKams();
    },
    consultarCanalesUsuario: function () {
        agentesControl.consultarCanalesUsuario(thatAgentes.consultarCanalesUsuarioCallback);
    },
    consultarCanalesUsuarioCallback: function (data) {
        var opciones = [];
        for (var i = 0; i < data.datos.length; i++) {
            var opcion = $('<option>').val(data.datos[i].canal).html(data.datos[i].canal);
            opciones.push(opcion);
        }
        _dom.cargarSelect('#cboCanales', opciones, thatAgentes.cargarPuntosDisponibles);
    },
    cargarPuntosDisponibles: function () {
        if (_dom.obtenerValorSelect($(this)) === "") {
            $('#tblPuntos tbody').empty();
            $('#tblPremios tbody').empty();
            return;
        }
        agentesControl.consultarPuntosDisponibles({canal: _dom.obtenerValorSelect('#cboCanales')}, thatAgentes.consultarPuntosDisponiblesCallback);
        agentesControl.consultarPuntosAgentes({canal: _dom.obtenerValorSelect('#cboCanales')}, thatAgentes.consultarPuntosAgentesCallback);
    },
    consultarPuntosDisponiblesCallback: function (data) {
        var tablaPuntos = $('#tblPuntos').find('tbody');
        tablaPuntos.empty();
        var fila = $('<tr>');
        fila.append($('<td>').html(data.datos.canal));
        fila.append($('<td>').html(data.datos.disponibles));
        tablaPuntos.append(fila);
        agentesControl.consultarProductosDisponibles({canal: _dom.obtenerValorSelect('#cboCanales')}, thatAgentes.consultarProductosDisponiblesCallback);
    },
    consultarProductosDisponiblesCallback: function (data) {
        var tablaPremios = $('#tblPremios').find('tbody');
        tablaPremios.empty();
        for (var i = 0; i < data.datos.length; i++) {
            var premio = data.datos[i];
            var fila = $('<tr>');
            fila.append($('<td>').html(premio.canal));
            fila.append($('<td>').html(premio.descripcion));
            fila.append($('<td>').html(premio.puntos));
            tablaPremios.append(fila);
        }
        thatAgentes.cargarCajaPremios(data.datos);
    },
    cargarCajaPremios: function (datos) {
        var opciones = [];
        for (var i = 0; i < datos.length; i++) {
            var opcion = $('<option>').val(datos[i].descripcion).html(datos[i].descripcion).attr('data-puntos', datos[i].puntos);
            opciones.push(opcion);
        }
        _dom.cargarSelect('#cboPremios', opciones, null);
    },
    consultarKams: function () {
        agentesControl.consultarKams(thatAgentes.consultarKamsCallback);
    },
    consultarKamsCallback: function (data) {
        var opciones = [];
        for (var i = 0; i < data.datos.length; i++) {
            var kam = data.datos[i];
            var opcion = $('<option>').val(kam.nombre).html(kam.nombre);
            opciones.push(opcion);
        }
        _dom.cargarSelect('#cboKams', opciones, null);
    },
    consultarPuntosAgentesCallback: function (data) {
        var opciones = [];
        for (var i = 0; i < data.datos.length; i++) {
            var punto = data.datos[i];
            var opcion = $('<option>').val(punto.nombrePunto).html(punto.nombrePunto);
            opciones.push(opcion);
        }
        _dom.cargarSelect('#cboPuntos', opciones, null);
    },
    redimirPremio: function (e) {
        e.preventDefault();
        var canal = _dom.obtenerValorSelect('#cboCanales');
        var premio = _dom.obtenerValorSelect('#cboPremios');
        var punto = _dom.obtenerValorSelect('#cboPuntos');
        var kam = _dom.obtenerValorSelect('#cboKams');
        var recibe = $('#txtRecibe').val();
        var telefono =$('#txtTelefono').val();
        if (canal === null || premio === null || punto === null || kam === null) {
            _dom.mostrarVentanaError('Registro de premio', 'Debe diligenciar correctamente los campos del formulario para registrar el premio');
            return;
        }
        var redencion = {
            tipo: premio,
            recibe: recibe,
            punto: punto,
            kam: kam,
            puntos: _dom.obtenerAtributoSelect('#cboPremios', 'data-puntos'),
            autorizacion: 'n',
            telefono: telefono,
            canal: canal
        };
        agentesControl.insertarRedencion(JSON.stringify(redencion), thatAgentes.redimirPremioCallback);
    },
    redimirPremioCallback: function (data) {
        _dom.mostrarDialogo(data.mensaje);
        document.getElementById("formAgentes").reset();
        _dom.resetSelects(['#cboCanales', '#cboPremios', '#cboPuntos', '#cboKams']);
        $('#tblPuntos tbody').empty();
        $('#tblPremios tbody').empty();
    }
};
agentesVista.init();