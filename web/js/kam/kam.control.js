/* global _app, _rutas */
var kamControl = {
    consultarRedenciones: function (callback) {
        return _app.ajax({
            'url': _rutas.kam.encontrar,
            'completado': callback
        });
    },
    redimirRedencion: function (data, callback) {
        return _app.ajax({
            'url': _rutas.kam.redimir,
            'data': data,
            'completado': callback
        });
    }
};