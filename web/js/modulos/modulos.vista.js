var thatModulo = null;
var modulosVista = {
    init: function () {
        console.log("vista cargada");
        thatModulo = this;
        thatModulo.consultarModulos();
    },
    consultarModulos: function () {
        modulosControl.consultarModulos(thatModulo.consultarModulosCallback);
    },
    consultarModulosCallback: function (data) {
        moduloModelo.modulos = data.datos;
        for (var i = 0; i < moduloModelo.modulos.length; i++) {
            var modulo = moduloModelo.modulos[i];
            var contenedor = $('#modulos');
            contenedor.append(thatModulo.crearTemplateModulo(modulo, i));
        }
    },
    crearTemplateModulo: function (modulo, i) {
        // componentes base de la tarjeta
        var itemModulo = $('<div>').addClass('col s6 m4');
        var tarjeta = $('<div>').addClass('card hoverable').attr('data-id-modulo', modulo.idValidez);
        var cabeceraTarjeta = $('<div>').addClass('card-image lighten-1');
        thatModulo.ponerColor(i, cabeceraTarjeta);
        var imagenTarjeta = $('<img>').addClass('space-spacer').attr('src', 'img/modulos/imagen-' + i + '.png');
        var tituloTarjeta = $('<span>').addClass('card-title').html(modulo.tipo);
        var areaBoton = $('<div>').addClass('card-action row');
        var botonTarjeta = $('<a>').addClass('btn col s12 lighten-1 waves-effect waves-light').attr('data-estado', modulo.validez).attr('data-id-modulo', modulo.idValidez).html('Desactivar');
        thatModulo.ponerColor(i, botonTarjeta);
        itemModulo.append(tarjeta);
        tarjeta.append(cabeceraTarjeta);
        cabeceraTarjeta.append(imagenTarjeta);
        cabeceraTarjeta.append(tituloTarjeta);
        tarjeta.append(areaBoton);
        areaBoton.append(botonTarjeta);

        // logica de las tarjetas
        if (modulo.validez === 'n') {
            thatModulo.removerColor(cabeceraTarjeta);
            thatModulo.removerColor(botonTarjeta);
            botonTarjeta.html('').html('Activar');

        }
        botonTarjeta.on('click', thatModulo.cambiarEstadoModulo);

        return itemModulo;
    },
    cambiarEstadoModulo: function (e) {
        for (var i = 0; i < moduloModelo.modulos.length; i++) {
            var modulo = moduloModelo.modulos[i];
            if (modulo.idValidez.toString() === $(this).attr('data-id-modulo')) {
                thatModulo.registrarCambioEstadoModulo(modulo);
            }
        }
    },
    registrarCambioEstadoModulo: function (modulo) {
        if (modulo.validez === 's') {
            modulo.validez = 'n';
        } else {
            modulo.validez = 's';
        }
        modulosControl.modificarModulo(JSON.stringify(modulo), thatModulo.registrarCambioEstadoModuloCallback);
    },
    registrarCambioEstadoModuloCallback: function (data) {
        _dom.mostrarDialogo(data.mensaje);
        modulosControl.consultarModulos(thatModulo.consultarModulosActualizarCallback);
    },
    consultarModulosActualizarCallback: function (data) {
        moduloModelo.modulos = data.datos;
        var tarjetas = $('#modulos').find('.card');
        for (var i = 0; i < moduloModelo.modulos.length; i++) {
            var modulo = moduloModelo.modulos[i];
            var tarjeta = $(tarjetas[i]);
            var fondo = tarjeta.find('.card-image');
            var boton = tarjeta.find('.card-action').children();
            if (modulo.validez === 's') {
                thatModulo.ponerColor(i,fondo);
                thatModulo.ponerColor(i,boton);
            } else {
                thatModulo.removerColor(fondo);
                thatModulo.removerColor(boton);
            }
        }
    },
    ponerColor: function (i, contenedor) {
        contenedor.removeClass('grey');
        switch (i) {
            case 0:
                contenedor.addClass('blue');
                break;
            case 1:
                contenedor.addClass('green');
                break;
            case 2:
                contenedor.addClass('red');
                break;
        }
    },
    removerColor: function (contenedor) {
        contenedor.removeClass('blue green red');
        contenedor.addClass('grey');
    }
};
modulosVista.init();