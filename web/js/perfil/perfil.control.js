var perfilControl = {
    consultarPerfil: function (callback) {
        return _app.ajax({
            'url': _rutas.usuarios.consultar,
            'completado': callback
        });
    },
    buscarPerfil: function (data, callback) {
        return _app.ajax({
            'url': _rutas.usuarios.buscar,
            'data': data,
            'completado': callback
        });
    },
    modificarPerfil: function (data, callback) {
        return _app.ajax({
            'url': _rutas.usuarios.modificar,
            'data': data,
            'completado': callback
        });
    }
};