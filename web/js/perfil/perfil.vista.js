var thatPerfil = null;
var perfilVista = {
    init: function () {
        thatPerfil = this;
        console.log('perfil cargado');
        $('.tooltipped').tooltip({delay: 50});
        thatPerfil.consultarPerfilActual();
        $('#formModificar').on('submit', thatPerfil.editarPerfil);
    },
    consultarPerfilActual: function () {
        perfilControl.buscarPerfil({id: 0}, thatPerfil.consultarPerfilActualCallback);
    },
    consultarPerfilActualCallback: function (data) {
        perfilModelo.perfilActual = data.datos;
        $('#txtCorreo').val(perfilModelo.perfilActual.email);
        $('#txtCelular').val(perfilModelo.perfilActual.celular);
        Materialize.updateTextFields();
    },
    editarPerfil: function (e) {
        e.preventDefault();
        console.log('se envio');
        var contrasena = $('#txtContrasena').val();
        var repetirContrasena = $('#txtRepetirContrasena').val();
        if ((contrasena !== '' && repetirContrasena === '') || (contrasena === '' && repetirContrasena !== '')) {
            _dom.mostrarVentanaError('Valor de contraseñas', 'Para cambiar la contraseña debes diligenciar correctamente el formulario');
            return;
        }
        perfilModelo.perfilActual.email = $('#txtCorreo').val();
        perfilModelo.perfilActual.celular = $('#txtCelular').val();
        if (contrasena !== '' && contrasena === repetirContrasena) {
            perfilModelo.perfilActual.contrasena = contrasena;
        }
        perfilControl.modificarPerfil(JSON.stringify(perfilModelo.perfilActual), thatPerfil.editarPerfilCallback);
    },
    editarPerfilCallback: function (data) {
        _dom.mostrarDialogo(data.mensaje);
    }
};
perfilVista.init();


