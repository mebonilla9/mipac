<%-- 
    Document   : modulos
    Created on : 29-ene-2018, 11:14:20
    Author     : Lord_Nightmare
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="contenedorModulos">
    <nav>
        <div class="nav-wrapper blue darken-1">
            <div class="col s12" style="margin-left: 10px;">
                <a href="#!" class="breadcrumb">Administración</a>
                <a href="#!" class="breadcrumb">Modulos</a>
            </div>
        </div>
    </nav>
    <div class="separador"></div>
    <div class="card grey lighten-5">
        <div class="row" id="modulos">

        </div>
    </div>
</div>

<script src="js/modulos/modulos.modelo.js"></script>
<script src="js/modulos/modulos.control.js"></script>
<script src="js/modulos/modulos.vista.js"></script>



