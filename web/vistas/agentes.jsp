<%-- 
    Document   : agentes
    Created on : 29-ene-2018, 11:17:59
    Author     : Lord_Nightmare
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="contenedorAgente">
    <form id="formAgentes">
        <nav>
            <div class="nav-wrapper blue darken-1">
                <div class="col s12" style="margin-left: 10px;">
                    <a href="#!" class="breadcrumb">Agentes</a>
                </div>
            </div>
        </nav>
        <div class="separador"></div>
        <div class="card grey lighten-5">
            <div class="row">
                <div class="input-field col s12">
                    <select id="cboCanales" required>
                        <option value=" " disabled selected>Seleccione...</option>
                    </select>
                    <label>Seleccione Canal</label>
                </div>
            </div>
        </div>
        <div class="separador"></div>
        <div class="card grey lighten-5">
            <div class="row">
                <div class="col s6">
                    <table id="tblPuntos" class="striped responsive-table">
                        <caption>Puntos disponibles</caption>
                        <thead>
                        <th>Canal</th>
                        <th>Disponible</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="col s6">
                    <table id="tblPremios" class="striped responsive-table">
                        <caption>Premios disponibles</caption>
                        <thead>
                        <th>Canal</th>
                        <th>Descripcion</th>
                        <th>Puntos</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="separador"></div>
        <div class="card grey lighten-5">
            <div class="row">
                <div class="input-field col s12">
                    <select id="cboPremios" required>
                        <option value=" " disabled selected>Seleccione...</option>
                    </select>
                    <label>Seleccione Premio</label>
                </div>
                <div class="input-field col m6 s12">
                    <label for="txtRecibe">Quien recibe</label>
                    <input type="text" id="txtRecibe" class="validate" required>
                </div>
                <div class="input-field col m6 s12">
                    <label for="txtTelefono">Teléfono Contacto</label>
                    <input type="text" id="txtTelefono" class="validate" required>
                </div>
                <div class="input-field col s12">
                    <select id="cboPuntos" required>
                        <option value=" " disabled selected>Seleccione...</option>
                    </select>
                    <label>Seleccione Punto</label>
                </div>
                <div class="input-field col s12">
                    <select id="cboKams" required>
                        <option value=" " disabled selected>Seleccione...</option>
                    </select>
                    <label>Seleccione Kam</label>
                </div>
            </div>
        </div>
        <div class="fixed-action-btn">
            <button type="submit" class="btn-floating btn-large green">
                <i class="large material-icons">add</i>
            </button>
        </div>
    </form>

</div>

<script type="text/javascript" src="js/agentes/agentes.modelo.js"></script>
<script type="text/javascript" src="js/agentes/agentes.control.js"></script>
<script type="text/javascript" src="js/agentes/agentes.vista.js"></script>
