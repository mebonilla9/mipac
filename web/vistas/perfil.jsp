<%-- 
    Document   : perfil
    Created on : 31-ene-2018, 10:24:14
    Author     : Lord_Nightmare
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="contenedorModulos">
    <nav>
        <div class="nav-wrapper blue darken-1">
            <div class="col s12" style="margin-left: 10px;">
                <a href="#!" class="breadcrumb">Administración</a>
                <a href="#!" class="breadcrumb">Actualizar Perfil</a>
            </div>
        </div>
    </nav>

    <div class="separador"></div>
    <form id="formModificar">
        <div class="card grey lighten-5">
            <div class="row">
                <div class="input-field col m6 s12">
                    <label for="txtCorreo">Correo</label>
                    <input type="email" placeholder="correo usuario" id="txtCorreo" class="validate" required>
                </div>
                <div class="input-field col m6 s12">
                    <label for="txtCelular">Celular</label>
                    <input type="tel" placeholder="telefono usuario" id="txtCelular" class="validate" required>
                </div>
            </div>
        </div>
        <div class="card grey lighten-5">
            <div class="row">
                <div class="input-field col m6 s12">
                    <label for="txtContrasena">Nueva Contraseña</label>
                    <input type="password" id="txtContrasena" class="validate">
                </div>
                <div class="input-field col m6 s12">
                    <label for="txtRepetirContrasena">Repetir Contraseña</label>
                    <input type="password" id="txtRepetirContrasena" class="validate">
                </div>
            </div>
        </div>
        <div class="fixed-action-btn">
            <button type="submit" class="btn-floating btn-large red tooltipped" data-position="left" data-delay="50" data-tooltip="Clic aqui para editar el perfil">
                <i class="large material-icons">mode_edit</i>
            </button>
        </div>
    </form>
</div>

<script type="text/javascript" src="js/perfil/perfil.modelo.js"></script>
<script type="text/javascript" src="js/perfil/perfil.control.js"></script>
<script type="text/javascript" src="js/perfil/perfil.vista.js"></script>
