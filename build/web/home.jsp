<%-- 
    Document   : home
    Created on : 19-oct-2017, 20:01:47
    Author     : instructor
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="css/clubelite.css">
    </head>
    <body>
        <header class="navbar-fixed">
            <nav blue-grey darken-2>
                <div class="nav-wrapper blue darken-1">
                    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
                    <a href="#" class="brand-logo ellipsis"><i class="material-icons icono-app">stars</i>MIPAC</a>
                    <ul class="right hide-on-med-and-down">
                        <li>
                            <a href="cerrar"><i class="material-icons right">exit_to_app</i>Cerrar sesión</a>
                        </li>
                    </ul>
                </div>
                <ul id="slide-out" class="side-nav fixed top-navbar">
                    <li>
                        <div class="user-view">
                            <div class="background">
                                <img src="img/background.jpg">
                            </div>
                            <a href="#!user"><img class="circle" src="img/man_128.png"></a>
                            <a href="#!name"><span class="white-text name"><c:out value="${sessionScope.usuario.nombre}"/></span></a>
                            <a href="#!email">
                                <c:choose>
                                    <c:when test="${sessionScope.usuario.email != null}">
                                        <span class="white-text email">
                                            <c:out value="${sessionScope.usuario.email}"/>
                                        </span>
                                    </c:when>    
                                    <c:otherwise>
                                        <span class="white-text email">
                                            <c:out value="${sessionScope.usuario.cedula}"/>
                                        </span>
                                    </c:otherwise>
                                </c:choose>
                            </a>
                        </div>
                    </li>
                    <c:if test="${sessionScope.usuario.tipo == 'x'}">
                        <li>
                            <a class="subheader">Administración</a>
                        </li>
                        <li><div class="divider"></div></li>
                        <li>
                            <a id="miModulos" class="waves-effect menu-element"><i class="material-icons">apps</i>Modulos</a>
                        </li>
                        <li>
                            <a id="miRegistrar" class="waves-effect menu-element"><i class="material-icons">person_add</i>Registrar Usuario</a>
                        </li>
                        <li>
                            <a id="miMeses" class="waves-effect menu-element"><i class="material-icons">chrome_reader_mode</i>Meses</a>
                        </li>
                        <li>
                            <a id="miPuntos" class="waves-effect menu-element"><i class="material-icons">blur_on</i>Puntos</a>
                        </li>
                    </c:if>
                    <c:if test="${sessionScope.usuario.tipo != 'x'}">
                        <c:forEach items="${sessionScope.validos}" var="valido">
                            <c:if test="${sessionScope.usuario.tipo == 'a' && valido.tipo == 'Agentes' && valido.validez == 's'}">
                                <li>
                                    <a id="mi${valido.tipo}" class="waves-effect menu-element"><i class="material-icons">account_box</i>${valido.tipo}</a>
                                </li>
                                <li>
                                    <a id="miMecanica" class="waves-effect menu-element"><i class="material-icons">chrome_reader_mode</i>Mecanica Agentes</a>
                                </li>
                            </c:if>
                            <c:if test="${sessionScope.usuario.tipo == 'k' && valido.tipo == 'Kam' && valido.validez == 's'}">
                                <li>
                                    <a id="mi${valido.tipo}" class="waves-effect menu-element"><i class="material-icons">supervisor_account</i>${valido.tipo}</a>
                                </li>
                            </c:if>
                        </c:forEach>
                    </c:if>
                    <li><div class="divider"></div></li>
                    <li>
                        <a id="miPerfil" class="waves-effect menu-element"><i class="material-icons">account_box</i>Perfil</a>
                    </li>
                    <li><div class="divider"></div></li>
                    <li class="item-cerrar-side-nav">
                        <a href="cerrar"><i class="material-icons right">exit_to_app</i>Cerrar sesión</a>
                    </li>
                </ul>
            </nav>
        </header>
        <main>
            <div class="contenedor-principal">
                <!--Aqui van los archivos jsp de cada una de las tablas del dashboard--> 
            </div>
            <!-- Modal Structure -->
            <div id="ventanaModal" class="modal">
                <div class="modal-content">
                    <h4 id="modalTitle"></h4>
                    <p id="modalBody">A bunch of text</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat">Aceptar</a>
                </div>
            </div>
        </main>
        <script type="text/javascript" src="js/libs/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/libs/picker.js"></script>
        <script type="text/javascript" src="js/libs/picker.date.js"></script>
        <script type="text/javascript" src="js/libs/picker.time.js"></script>
        <script type="text/javascript" src="js/libs/materialize.min.js"></script>
        <script type="text/javascript" src="js/libs/sha256.js"></script>
        <script type="text/javascript" src="js/app/app.rutas.js"></script>
        <script type="text/javascript" src="js/app/app.dom.js"></script>
        <script type="text/javascript" src="js/app/app.ajax.js"></script>
        <script type="text/javascript" src="js/home/home.vista.js"></script>
    </body>
</html>
