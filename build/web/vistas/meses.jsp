<%-- 
    Document   : meses
    Created on : 29-ene-2018, 14:10:41
    Author     : Lord_Nightmare
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="contenedor-meses">
    <nav>
        <div class="nav-wrapper blue darken-1">
            <div class="col s12" style="margin-left: 10px;">
                <a href="#!" class="breadcrumb">Administración</a>
                <a href="#!" class="breadcrumb">Meses</a>
            </div>
        </div>
    </nav>
    <div class="separador"></div>
    <form id="formMeses">
        <div class="card grey lighten-5">

            <div class="row">
                <div class="input-field col s12">
                    <select id="cboObtenibles" multiple>
                        <option value="0" disabled selected>Selecciona...</option>
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                    </select>
                    <label>Meses a redimir</label>
                </div>
                <div class="input-field col s12">
                    <select id="cboRedimibles" multiple>
                        <option value="0" disabled selected>Selecciona...</option>
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                    </select>
                    <label>Meses a descontar</label>
                </div>
            </div>
            <div class="fixed-action-btn">
                <button type="submit" class="btn-floating btn-large red tooltipped" data-position="left" data-delay="50" data-tooltip="Clic aqui para registrar meses">
                    <i class="large material-icons">add</i>
                </button>
            </div>

        </div>
    </form>
</div>

<script type="text/javascript" src="js/meses/meses.modelo.js"></script>
<script type="text/javascript" src="js/meses/meses.control.js"></script>
<script type="text/javascript" src="js/meses/meses.vista.js"></script>

