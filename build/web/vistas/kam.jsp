<%-- 
    Document   : kam
    Created on : 29-ene-2018, 11:17:28
    Author     : Lord_Nightmare
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="contenedorKam">
    <nav>
        <div class="nav-wrapper blue darken-1">
            <div class="col s12" style="margin-left: 10px;">
                <a href="#!" class="breadcrumb">Kam</a>
                <a href="#!" class="breadcrumb">Redención</a>
            </div>
        </div>
    </nav>
    <div class="separador"></div>
    <div class="card grey lighten-5">
        <table id="tblRedenciones" class="responsive-table">
            <thead>
            <th>Tipo</th>
            <th>Recibe</th>
            <th>Punto</th>
            <th>Puntos</th>
            <th>Aprobar</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="js/kam/kam.modelo.js"></script>
<script type="text/javascript" src="js/kam/kam.control.js"></script>
<script type="text/javascript" src="js/kam/kam.vista.js"></script>
