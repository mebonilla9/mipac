<%-- 
    Document   : registrar
    Created on : 31-oct-2017, 21:24:45
    Author     : lord_nightmare
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="contenedorUsuario">
    <nav>
        <div class="nav-wrapper blue darken-1">
            <div class="col s12" style="margin-left: 10px;">
                <a href="#!" class="breadcrumb">Administración</a>
                <a href="#!" class="breadcrumb">Registrar Usuario</a>
            </div>
        </div>
    </nav>
    <div class="separador"></div>
    <div class="card grey lighten-5">
        <form id="formRegistrar">
            <div class="row">
                <div class="input-field col m6 s12">
                    <label for="txtNombre">Nombre</label>
                    <input type="text" id="txtNombre" class="validate" required>
                </div>
                <div class="input-field col m6 s12">
                    <label for="txtIdentificacion">Identificacion</label>
                    <input type="text" id="txtIdentificacion" class="validate" required>
                </div>
                <div class="input-field col m6 s12">
                    <select id="cboTipo">
                        <option value="">Seleccione un tipo de usuario...</option>
                        <option value="a">Agente</option>
                        <option value="k">Kam</option>
                        <option value="d">Administrador</option>
                    </select>
                </div>
                <div class="input-field col m6 s12">
                    <select id="cboCanal">
                        <option value="">Seleccione un tipo de canal...</option>
                        <option value="agente">Agente</option>
                        <option value="retail">Retail</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="input-field col m5 offset-m7 s12">
                    <div class="row">
                        <div class="input-field col s12">
                            <button type="submit" id="btnGuardar" style="float: right;" class="btn blue lighten-1 waves-effect waves-light">Guardar</button>
                        </div>
                        <div class="input-field col s12">
                            <button type="reset" style="float: right;" class="btn red waves-effect waves-light">Limpiar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="js/registrar/registrar.modelo.js"></script>
<script type="text/javascript" src="js/registrar/registrar.control.js"></script>
<script type="text/javascript" src="js/registrar/registrar.vista.js"></script>
