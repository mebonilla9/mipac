var _rutas = {
    modulos: {
        modificar: '/validez/editar',
        consultar: '/validez/consultar'
    },
    usuarios: {
        insertar: '/usuarios/insertar',
        modificar: '/usuarios/editar',
        buscar: '/usuarios/buscar',
        kams: '/usuarios/kam'
    },
    meses: {
        registrar: '/meses/registrar'
    },
    kam: {
        redimir: '/redencionagentes/aprobar',
        encontrar: '/redencionagentes/encontrar',
        nuevo:'/redencionagentes/nuevo'
    },
    puntosObtenidos: {
        canal: '/puntosobtenidos/canal'
    },
    puntosAgentes: {
        consultar: '/puntosagentes/consultar',
        canal: '/puntosagentes/canal'
    },
    puntosDisponibles: {
        puntosCanal: '/puntosdisponibles/canal'
    },
    catalogoAgentes: {
        consultar: '/catalogoagentes/consultar'
    }
};