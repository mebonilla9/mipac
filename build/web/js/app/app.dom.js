/* global Materialize */

var _dom = {
    mostrarCargador: function () {
        // instruccion para mostrar dialogo
    },
    ocultarCargador: function () {
        // instruccion para cerrar dialogo
    },
    mostrarPanelError: function (msg) {
        Materialize.Toast.removeAll();
        Materialize.toast(msg, 5000, 'red');
    },
    mostrarVentanaError: function (titulo, mensaje) {
        $('#modalTitle').html(titulo);
        $('#modalBody').html(mensaje);
        $('.modal').modal('open');
    },
    mostrarDialogo: function (msg) {
        Materialize.Toast.removeAll();
        Materialize.toast(msg, 6000, 'green');
    },
    obtenerValorSelect: function (selector) {
        var value = '';
        var cbo = $(selector);
        cbo.material_select('destroy');
        value = cbo.val();
        cbo.material_select();
        return value;
    },

    obtenerAtributoSelect: function (selector, atributo) {
        var value = '';
        var cbo = $(selector);
        cbo.material_select('destroy');
        value = cbo.find(":selected").attr(atributo);
        cbo.material_select();
        return value;
    },
    asignarValorSelect: function (selector, value) {
        var cbo = $(selector);
        cbo.material_select('destroy');
        cbo.val(value);
        cbo.material_select();
    },
    iniciarSelect: function (selector) {
        var cbo = $(selector);
        cbo.material_select();
    },
    cargarSelect: function (selector, opciones, evento) {
        var cbo = $(selector);
        cbo.material_select('destroy');
        for (var i = 0; i < opciones.length; i++) {
            cbo.append(opciones[i]);
        }
        if (evento !== null || evento !== undefined) {
            cbo.on('change', evento);
        }
        cbo.material_select();
    },
    resetSelects: function (array) {
        for (var i = 0; i < array.length; i++) {
            var cbo = $(array[i]);
            cbo.material_select('destroy');
            cbo.prop('selectedIndex', 0);
            cbo.material_select();
        }
    }
};