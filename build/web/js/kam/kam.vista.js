var thatKam = null;
var kamVista = {
    init:function(){
        thatKam = this;
        thatKam.consultarRedenciones();
    },
    consultarRedenciones:function(){
        kamControl.consultarRedenciones(thatKam.consultarRedencionesCallback);
    },
    consultarRedencionesCallback:function(data){
        var cuerpoTabla = $('#tblRedenciones').find('tbody');
        for (var i = 0; i < data.datos.length; i++) {
            var registro = data.datos[i];
            cuerpoTabla.append(thatKam.armarFilaTabla(registro));
        }
        cuerpoTabla.find('button').on('click',thatKam.confirmarRedencion);
    },
    armarFilaTabla:function(registro){
        console.log(registro);
        var fila = $('<tr>');
        fila.append($('<td>').html(registro.tipo));
        fila.append($('<td>').html(registro.recibe));
        fila.append($('<td>').html(registro.punto));
        fila.append($('<td>').html(registro.puntos));
        var btnAprobar = $('<button>').addClass('btn waves-effect green waves-light').attr('data-aprove','s').attr('data-id',registro.idRedencion);
        var btnRechazar = $('<button>').addClass('btn waves-effect red waves-light').attr('data-aprove','n').attr('data-id',registro.idRedencion);
        var celdaBoton = $('<td>').append(btnAprobar).append(btnRechazar);
        fila.append(celdaBoton);
        return fila;
    },
    confirmarRedencion:function(){
        var valor = $(this).attr('data-aprove');
        var id = $(this).attr('data-id');
        kamControl.redimirRedencion({id:id,valor:valor},thatKam.confirmarRedencionCallback);
    },
    confirmarRedencionCallback:function(data){
        var cuerpoTabla = $('#tblRedenciones').find('tbody');
        cuerpoTabla.empty();
        thatKam.consultarRedenciones();
        _dom.mostrarDialogo(data.mensaje);
    }
};
kamVista.init();