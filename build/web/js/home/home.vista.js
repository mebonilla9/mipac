/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var homeThat;
var homeVista = {
    init: function () {
        homeThat = this;
        console.log('Home Cargado');
        $('.modal').modal();
        $(".button-collapse").sideNav();
        $('#slide-out').find('.menu-element').on('click', homeThat.navegarItem);
    },
    navegarItem: function () {
        var id = $(this).attr('id');
        var componente = '';
        $('.contenedor-principal').empty();
        switch (id) {
            case 'miModulos':
                componente = 'vistas/modulos.jsp';
                break;
            case 'miRegistrar':
                componente = 'vistas/registrar.jsp';
                break;
            case 'miMeses':
                componente = 'vistas/meses.jsp';
                break;
            case 'miPuntos':
                componente = 'vistas/puntos.jsp';
                break;
            case 'miAgentes':
                componente = 'vistas/agentes.jsp';
                break;
            case 'miKam':
                componente = 'vistas/kam.jsp';
                break;
            case 'miPerfil':
                componente = 'vistas/perfil.jsp';
                break;
            case 'miMecanica':
                componente = 'vistas/mecanica.jsp';
                break;
            default:
                componente = 'vistas/default.jsp';
                break;
        }
        $('.button-collapse').sideNav('hide');
        $('.contenedor-principal').load(componente);
    }
};
homeVista.init();

