var modulosControl = {
    consultarModulos: function (callback) {
        return _app.ajax({
            'url': _rutas.modulos.consultar,
            'completado': callback
        });
    },
    modificarModulo:function(data,callback){
        return _app.ajax({
            'url': _rutas.modulos.modificar,
            'data': data,
            'completado': callback
        });
    }
};